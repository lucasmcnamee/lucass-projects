==================================================================
Project 3: Huffman Encoding

@Author Luke Moran McNamee
@Version 2016.10.25 "A Level(Complete)"

This project consists of 2 programs "huff" and "unhuff". These two
programs compress and decompress (respectively) using Huffman 
Encoding.

To run use the command prompt to run the program. Type in a file
which is located in the same folder as the program, I have 
provided a sample "foo.txt." The program will print the some tests
but for the purposes of usability most of the numerous tests have 
been commented out of the code. Feel free to use them however you 
wish, however note that some will dramatically increase the 
running time of the program.
When the program is run, the final output (for both of the 
programs will be stored in a file called:
 	"[the original file name]_huffed" (or unhuff respectively)
==================================================================
Progress: 
    Part I:
	ReadFile: D level Complete
	Main: D level Complete
    Part II:
	ReadFile: C level Complete
	HuffmanNode: C level Complete
	HeapNode: C level Complete
    Part III: 
	ReadFile: B level Complete
    Part IV:
	ReadFile: A level Complete
