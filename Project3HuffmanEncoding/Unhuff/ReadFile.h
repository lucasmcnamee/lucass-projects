#ifndef READFILE_H
#define READFILE_H

#include "HeapNode.h"
#include "HuffmanNode.h"

#include <fstream>
#include <queue>
#include <vector>
#include <array>
/*
 * Class ReadFile - given a 'huffed' file this class converts the huffed bits
 *  back into characters (using the bitops class courtesy of Owen Astrachan) and
 *  then reverses the 'huff' process to decompress the file. inside of a Huffman
 *  tree using the HeapNode class to
 *
 *  The class will create a new file called '[original file name]_huffed.txt'
 *  where it stores the encoded file.
 *
 * @author Luke Moran McNamee (with contribution from Gary Lewandowski and Owen Astrachan)
 * @version 2016.11.07
 *    "A Level(Complete)"
 */
 using namespace std;

 class ReadFile
 {

  private:
    const int PSEUDOEOF = 256;
    // Holds a value larger than an ascii character to signal to the program to stop reading the file
    priority_queue<HeapNode>  theHeap;
    // A psuedo heap which will order the values of HeapNodes based on their values
    string finalPath = "";

    vector<string> charEncoding;

    vector<int> countedChars; // initialize the array which will store the frequency of characters

    /**
     * testPrint - for testing purposes, prints all of the characters which
     * appear at least once in the file with their frequency
     */
    void testPrint();

    /**
     * buildTree - builds a HuffmanTree out of the values from the frequencies
     * of characters in the countedChars array. After creating the tree it calls
     * the traverse function.
     */
    void buildTree();

void traverse(HuffmanNode* node, string path);

    /**
     * traverseTree - Traversal of the HuffmanTree. Using the encoding string
     * will traverse left or right until it reaches a node with a valid value.
     * Then add the path to that value to a vector with the index being the
     * value of the character.
     * @param node - the node at the root of the HuffmanTree.
     *        encoding - the string retrieved by the ofstream
     */
    string traverseTree(HuffmanNode* node, string encoding);
  public:
     ReadFile();

  };
#endif
