#include <fstream>
#include <vector>
#include "ReadFile.h"
#include <array>
#include <iostream>
#include <string>
#include "bitops.h"
/*
 * Class ReadFile - given a 'huffed' file this class converts the huffed bits
 *  back into characters (using the bitops class courtesy of Owen Astrachan) and
 *  then reverses the 'huff' process to decompress the file. inside of a Huffman
 *  tree using the HeapNode class to
 *
 *  The class will create a new file called '[original file name]_huffed.txt'
 *  where it stores the encoded file.
 *
 * @author Luke Moran McNamee (with contribution from Gary Lewandowski and Owen Astrachan)
 * @version 2016.11.07
 *    "A Level(Complete)"
 */
 using namespace std;

ReadFile::ReadFile()
{
   // Initialize and set the array to the needed constraints
   array<int, 257> countedChars; // initialize the array which will store the frequency of characters
   countedChars.fill(0); // set each value in the array to be 0
   countedChars[256] = 1; // sets the PSUEDOEOF

   charEncoding.resize(257, "");


   // Allows the user to select which file to read
   string fileName;
   cout << "Please name the file to read" << endl << "> ";
   cin >> fileName;

   // Using the string of the user input open the file with that string name
   ibstream infile;
   infile.open(fileName.c_str());

   string encodingPath = "";


    int numberOfNonZeroChars = 0;
    infile.readbits(32, numberOfNonZeroChars);
  std::cout << numberOfNonZeroChars << std::endl;

    int i = 0;
    while(i < numberOfNonZeroChars)
    {
      int character;
      int frequency;
        infile.readbits(8, character);
      //  cout << character << endl;
        infile.readbits(32, frequency);
      //  cout << frequency << endl;
      countedChars[character] = frequency;
      i++;
    }
      int inbits;
      int go = infile.readbits(1, inbits);
      while(go)
      {
        if(inbits == 0) encodingPath = encodingPath + "0";
        else encodingPath = encodingPath + "1";
        go = infile.readbits(1, inbits);
        if(go == 0) cout << "go = " << go << " (this means the bitops class stopped reading the string)" << endl;
      }
    //  std::cout << "encodingPath.length()" << encodingPath.length() << ": " << endl;
    //  cout << encodingPath << std::endl;
      infile.close();

   testPrint();

   for(int i = 0; i < countedChars.size(); i++)
   {
     if(countedChars[i]!=0) cout << (char) i << ": " << countedChars[i] << endl;
   }
   // add all of the nodes to the heap
   HeapNode heapNoding; // a HeapNode used to instantiate HuffmanNodes
   for(int i = 0; i <= countedChars.size()-1; i++) {
     if(countedChars[i] != 0) {
       heapNoding.buildLeaf(countedChars[i], i);
       theHeap.push(heapNoding);
     }
   }
   buildTree();
//   traverseTree(theHeap.top(), encodingPath);
string thing = "";
traverse(theHeap.top().value(), thing);

   ofstream myfile;
   myfile.open ("Decompressed_" + fileName);

   myfile << traverseTree(theHeap.top().value(), encodingPath);
   myfile.close();

   }

   /**
    * testPrint - for testing purposes, prints all of the characters which
    * appear at least once in the file with their frequency
    */
   void ReadFile::testPrint()
   {
     for(int i = 0; i < countedChars.size(); i++)
     {
       if(countedChars[i] != 0) {
         if(i == PSEUDOEOF)
          cout << "I've reached PSEUDOEOF: " << countedChars[i] << endl;

         else if(i == 32)
           cout << "I've reached (space): " << countedChars[i] << endl;

         else if(i == 10)
            cout << "I've reached a new line: " << countedChars[i] << endl;

         else cout << "I've reached " <<  i << " " << countedChars[i] << endl;
       }
     }
   }

   /**
    * buildTree - builds a HuffmanTree out of the values from the frequencies
    * of characters in the countedChars array. After creating the tree
    */
    void ReadFile::buildTree()
    {
      HeapNode heapNoding;
      while(theHeap.size() > 1) { // while the queue is not yet a single tree
        HuffmanNode* node1 = theHeap.top().value(); // instantiate the left child
        theHeap.pop();

        HuffmanNode* node2 = theHeap.top().value(); // instantiate the right child
        theHeap.pop();

        heapNoding.buildNode(node1, node2); // instantiate the parent with the two children
        theHeap.push(heapNoding);
      }
    }

    void ReadFile::traverse(HuffmanNode* node, string path)
    {
      if(node==NULL)
        return;
      if(node->Left() != NULL)
        traverse(node->Left(), path + "0");
      if(node->getValue() != -1 && node->getWeight() != 0) {
        charEncoding[node->getValue()] = path;
        if(node->getValue() == PSEUDOEOF)
         cout << "I've reached PSEUDOEOF at " << path << endl;
        else if(node->getValue() == 32)
          cout << "I've reached (space) at " << path << endl;
        else if(node->getValue() == 10)
           cout << "I've reached a new line at " << path << endl;
        else cout << "I've reached "  << (char) node->getValue() << " at " << path << endl;
      }
      std::cout << "start here. PSEUDOEOF " << charEncoding[256] << std::endl;
      if(node->Right() != NULL)
        traverse(node->Right(), path + "1");
    }

    /**
     * traverseTree - Traversal of the HuffmanTree. Using the encoding string
     * will traverse left or right until it reaches a node with a valid value.
     * Then add the path to that value to a vector with the index being the
     * value of the character.
     * @param node - the node at the root of the HuffmanTree.
     *        encoding - the string retrieved by the ofstream
     */
    string ReadFile::traverseTree(HuffmanNode* node, string encoding)
    {
      HuffmanNode* top = node;
      string returnMe = "";
      std::cout << "encoding.Length() " << encoding.length() << std::endl;
      for(int i = 0; i < encoding.length(); i++)
      {
      //  cout << "i = " << encoding[i] << endl;

        if(node->getValue() != -1) {
          if(node->getValue() == PSEUDOEOF)
          {
            std::cout << "pseudoeof" << std::endl;
            return returnMe;
          }
          returnMe = returnMe + (char) node->getValue();
        //  cout << "node != -1"  << endl;
      //  cout << returnMe << endl;
          node = top;
          i--; // subtract one from i because it does not follow the path this iteration
        }
        else if(encoding[i] == '0') {
          node = node->Left();
        //  cout << "going left" << endl;
        }
        else if(encoding[i] == '1') {
          node = node->Right();
        //  cout << "going right" << endl;
        }
      }
      std::cout << returnMe.length() << std::endl;
      return returnMe;
    }
