#include "ReadFile.h"
#include "HeapNode.h"
#include "HuffmanNode.h"
#include <iostream>
/*
 * Runs the ReadFile, HuffmanNode, and HeapNode Classes tests
 *
 * @author Luke Moran McNamee
 * @version 2016.10.24
 *    Complete (Target grade level N/A)
 */
using namespace std;

int main()
{
  // instantiates and runs ReadFile
  ReadFile readFile;
  return 1;
}
