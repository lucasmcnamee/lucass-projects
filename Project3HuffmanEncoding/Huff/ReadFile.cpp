#include <fstream>
#include <vector>
#include "ReadFile.h"
#include <array>
#include <iostream>
#include <string>
/*
 * Class ReadFile - Reads a given file and then stores the frequency of each
 *  character in the file inside of a Huffman tree using the HeapNode class to
 *  store the Huffman tree and the HuffmanNode class to store the character and
 *  frequency of that character within the file. When this class is run it will
 *  print the 'path' of each HuffmanNode in the form of a string with 1's and
 *  0's to locate where the file is. The class will create a new file called
 *  '[original file name]_huffed.txt' where it stores the encoded file.
 *
 *  When outputing into the outfile we use the 'bitops' class to convert the
 *  output into bits so as to use the least storeage possible. The bitops class
 *  was provided by Owen Astrachan and all credit for its creation go
 *  exclusively to him.
 *
 * @author Luke Moran McNamee (with contribution from Gary Lewandowski and Owen Astrachan)
 * @version 2016.11.07
 *    "A Level(Complete)"
 */
 using namespace std;

ReadFile::ReadFile()
{
   // Set the vector to the needed constraints
   countedChars.resize(257, 0); // set each value in the array to be 0
   countedChars[256] = 1; // sets the PSUEDOEOF

   // Allows the user to select which file to read
   string fileName;
   cout << "Please name the file to read" << endl << "> ";
   cin >> fileName;

   readInFile(fileName);

   int size_of_outfile = writeFileInBits(fileName.substr(0, fileName.find("."))  + "_huffed.txt");
   size_of_outfile = convert(size_of_outfile, 8);

   int size_of_infile = 0;
   for(int i = 0; i < countedChars.size()-1; i++)
   {
      if(countedChars[i] != 0)
        size_of_infile = size_of_infile + countedChars[i];
   }
    size_of_infile = convert(size_of_infile, 1024);
    size_of_outfile = convert(size_of_outfile, 1024);

    cout << endl;
    cout << "This compression took a file of size " << size_of_infile << "kb and compressed it to be " << size_of_outfile << " kb" << endl;
}

  /**
   * readInFile - takes the input of the fileName and uses ifstream to read in
   * the file by each character
   * @param strig - the fileName to readin
   */
  void ReadFile::readInFile(string fileName)
  {
    ifstream infile;
    infile.open(fileName.c_str());
    // while peeking ahead does not reveal end of filename
    while(infile.peek() && !infile.eof())
    {
      char h = infile.get();
      ++countedChars[h]; //increment the array on the index of the given character
    }
    infile.close();

    // add all of the nodes to the heap
    HeapNode heapNoding; // a HeapNode used to instantiate HuffmanNodes
    for(int i = 0; i <= countedChars.size()-1; i++) {
      if(countedChars[i] != 0) {
        finalCount++;

        heapNoding.buildLeaf(countedChars[i], i);
        theHeap.push(heapNoding);

        finalString = finalString + (char) i + to_string(countedChars[i]) + " ";
      }
    }
    finalCount--;
    /**
     * for(int i = 0; i < countedChars.size()-1; i++) {
     *  if(countedChars[i] != 0) cout << (char) i << countedChars[i] << endl;
     * }
     */

    buildTree();

    infile.open(fileName.c_str());
    while(infile.peek() && !infile.eof())
    {
      //cout << "I got here" << endl;
      int h = infile.get();
      //cout << "infile.get() " << (char) h << endl;
      finalPath = finalPath + charEncoding[h]; //increment the array on the index of the given character
      //cout << "adding " << (char) h << "to finalPath" << endl;
    }
    //  std::cout << finalPath.size() << std::endl;
  }

  /**
   * buildTree - this class is called once and it creates the HuffmanTree from
   * the values within the countedChars array. After creating the tree it calls
   * the traverse function.
   */
   void ReadFile::buildTree()
   {
     HeapNode heapNoding;
     while(theHeap.size() > 1) { // while the queue is not yet a single tree
       HuffmanNode* node1 = theHeap.top().value(); // instantiate the left child
       theHeap.pop();
       // cout << node1->getWeight() << endl;

       HuffmanNode* node2 = theHeap.top().value(); // instantiate the right child
       theHeap.pop();
       // cout << node2->getWeight() << endl;

       heapNoding.buildNode(node1, node2); // instantiate the parent with the two children
       theHeap.push(heapNoding);
       // cout << heapNoding.value()->getWeight() << endl;
     }
     charEncoding.resize(257, "");
     traverse(theHeap.top().value(), "");
   }

   /**
    * convert - used to convert the number of characters or bits which have been
    * counted into the number of bytes
    * @param   input - the int to be convert
    *          base - the base to which the input will be converted
    * @returns the converted input
    */
   int ReadFile::convert(int input, int base)
   {
     if(input % base != 0)
       input = (input / base) + 1;
       else
       input = input / base;
     return input;
   }

   /**
    * traverse - in-order traveral. Follows each path of the HuffMan tree to its
    * leaf.
    * @param node - the first node to find in the tree.
    *        path - a string passed in as ""
    */
   void ReadFile::traverse(HuffmanNode* node, string path)
   {
     if(node==NULL)
       return;
     if(node->Left() != NULL)
       traverse(node->Left(), path + "0");
     if(node->getValue() != -1 && node->getWeight() != 0) {
       charEncoding[node->getValue()] = path;
       if(node->getValue() == PSEUDOEOF)
        cout << "I've reached PSEUDOEOF at " << path << endl;
       else if(node->getValue() == 32)
         cout << "I've reached (space) at " << path << endl;
       else if(node->getValue() == 10)
          cout << "I've reached a new line at " << path << endl;
       else cout << "I've reached "  << (char) node->getValue() << " at " << path << endl;
     }
     //std::cout << "start here. PSEUDOEOF " << charEncoding[256] << std::endl;
     if(node->Right() != NULL)
       traverse(node->Right(), path + "1");
   }


   /**
    * writeFileInBits - given the string will convert the file into bits
    * @param string - the file to convert to bits
    * @returns int - the number of bits written to the outfile
    */
    int ReadFile::writeFileInBits(string infileName)
    {
      obstream outputFile;
      outputFile.open(infileName);
      outputFile.writebits(32, finalCount);
      int returnMe = 32;

      for(int i = 0; i < countedChars.size()-1; i++)
      {
        if(countedChars[i] != 0)
        {
          char letter = i;

          outputFile.writebits(8, letter);
            returnMe = returnMe + 8;
          outputFile.writebits(32, countedChars[i]);
            returnMe = returnMe + 32;
        }
      }
      int inbits;
      for(int i = 0; i < finalPath.size(); i++)
      {
        returnMe = returnMe + 1;
        if(finalPath[i] == '0'){
          outputFile.writebits(1, 0);
        //  std::cout << finalPath[i] << "0" << std::endl;
        }
        else {
          outputFile.writebits(1, 1);
          //std::cout <<finalPath[i] << "1" << std::endl;
        }
      }
      // std::cout << finalPath.length() << std::endl;
      outputFile.flushbits();
      outputFile.close();
      return returnMe;
    }
