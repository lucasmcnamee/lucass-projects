#ifndef READFILE_H
#define READFILE_H

#include "HeapNode.h"
#include "HuffmanNode.h"
#include "bitops.h"

#include <fstream>
#include <queue>
#include <array>
/*
 * Class ReadFile - Reads a given file and then stores the frequency of each
 *  character in the file inside of a Huffman tree using the HeapNode class to
 *  store the Huffman tree and the HuffmanNode class to store the character and
 *  frequency of that character within the file. When this class is run it will
 *  print the 'path' of each HuffmanNode in the form of a string with 1's and
 *  0's to locate where the file is. The class will create a new file called
 *  '[original file name]_huffed.txt' where it stores the encoded file.
 *
 *  When outputing into the outfile we use the 'bitops' class to convert the
 *  output into bits so as to use the least storeage possible. The bitops class
 *  was provided by Owen Astrachan and all credit for its creation go
 *  exclusively to him.
 *
 * @author Luke Moran McNamee (with contribution from Gary Lewandowski and Owen Astrachan)
 * @version 2016.11.07
 *    "A Level(Complete)"
 */
 using namespace std;

 class ReadFile
 {

  private:
    const int PSEUDOEOF = 256;
    // Holds a value larger than an ascii character to signal to the program to stop reading the file
    priority_queue<HeapNode> theHeap;
    // A psuedo heap which will order the values of HeapNodes based on their values

    vector<int> countedChars; // initialize the vector which will store the frequency of characters
    vector<string> charEncoding;

    // the next three varibles will be concatinated into the final body of the compressed file
    int finalCount = 0; // the number of nonzero characters in the file to be compressed
    string finalString = ""; // the pairs of characters and their frequencies in the file
    string finalPath = ""; // the compressed encoding string of 1's and 0's

    /**
     * readInFile - takes the input of the fileName and uses ifstream to read in
     * the file by each character
     * @param string - the fileName to readin
     */
    void readInFile(string fileName);

    /**
     * buildTree - this class is called once and it creates the HuffmanTree from
     * the values within the countedChars array. After creating the tree it calls
     * the traverse function.
     */
    void buildTree();

    /**
     * traverse - in-order traveral. Follows each path of the HuffMan tree to its
     * leaf.
     * @param node - the first node to find in the tree.
     *        path - a string passed in as ""
     */
    void traverse(HuffmanNode* node, string path);

    /**
     * convert - used to convert the number of characters or bits which have been
     * counted into the number of bytes
     * @param   input - the int to be convert
     *          base - the base to which the input will be converted
     * @returns the converted input
     */
    int convert(int input, int base);

    /**
     * writeFileInBits - given the string will convert the file into bits
     * @param string - the file to convert to bits
     * @returns int - the number of bits written to the outfile
     */
    int writeFileInBits(string infileName);
  public:
     ReadFile();
  };
#endif
