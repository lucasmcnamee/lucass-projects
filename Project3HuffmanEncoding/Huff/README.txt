==================================================================
Project 3: Huffman Encoding

@Author Luke Moran McNamee
@Version 2016.10.25 "B Level(Complete)"

This purpose of this program is to take a file through user input, 
read the file (using readfile), then count the frequency at which 
each character in the file appears. Later this will be improved 
into a full Huffman Encoding compression algorithm.

To run use the command prompt to run the program. Type in a file
which is located in the same folder as the program, I have 
provided a sample "foo.txt." The program will print the tests 
and the locations of the trees, given by a string of 1's and 0's.
When the program is run a file called example.txt will be created
in the folder, this will store the encoding.
==================================================================
Progress: 
    Part I:
	ReadFile: D level Complete
	Main: D level Complete
    Part II:
	ReadFile: C level Complete
	HuffmanNode: C level Complete
	HeapNode: C level Complete
    Part III: 
	ReadFile: B level Complete
