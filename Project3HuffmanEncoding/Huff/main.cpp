#include "ReadFile.h"
#include "HeapNode.h"
#include "HuffmanNode.h"
#include <iostream>
/*
 * Runs the ReadFile, HuffmanNode, and HeapNode Classes tests
 *
 * @author Luke Moran McNamee
 * @version 2016.10.24
 *    Complete (Target grade level N/A)
 */
using namespace std;

int main()
{
  // instantiates and runs ReadFile
  ReadFile readFile;

  // HuffmanNode and HeapNode Tests
  HeapNode heap1;
  HeapNode heap2;
  HeapNode heap3;

  heap1.buildLeaf(5,36);
  HuffmanNode* test1 = heap1.value();

  cout << "huff1" << endl;
  cout << "  value: " << test1->getValue() << " ascii value: " << (char) test1->getValue() << endl;
  cout << "  weight: " << test1->getWeight() << endl;
  cout << "  left node (if 0 then NULL): " << test1->Left() << endl;
  cout << "  right node (if 0 then NULL): " << test1->Right() << endl << endl;

  heap2.buildLeaf(15,37);
  HuffmanNode* test2 = heap2.value();

  cout << "huff2" << endl;
  cout << "  value: " << test2->getValue() << " ascii value: " << (char) test2->getValue() << endl;
  cout << "  weight " << test2->getWeight() << endl;
  cout << "  left node (if 0 then NULL): " << test2->Left() << endl;
  cout << "  right node (if 0 then NULL): " << test2->Right() << endl << endl;

  heap3.buildNode(test1, test2);
  HuffmanNode* test3 = heap3.value();

  cout << "huff3" << endl;
  cout << "  value: " << test3->getValue() << " ascii value: " << (char) test3->getValue() << endl;
  cout << "  weight " << test3->getWeight() << endl;
  cout << "  left node (if 0 then NULL): " << test3->Left() << endl;
  cout << "  right node (if 0 then NULL): " << test3->Right() << endl << endl;

  cout << "Compare two heaps using '<' operator: heap1 < heap2 = " << (heap1 < heap2) << endl;
}
