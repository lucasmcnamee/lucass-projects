#include <iostream>
#include "HuffmanNode.h"
/**
 * Class HuffmanNode -
 *
 * @author Luke Moran McNamee
 * @version 2016.10.20
 *    "C Level(Complete)"
 */
using namespace std;

  /**
   * HuffmanNode - this is the constructors to set up a leaf HuffmanNode
   *   the weight and value will be set to equal to the parameters given.
   *
   * @param setWeight - the weight of the node
   *        setValue - the value of the node
   */
  HuffmanNode::HuffmanNode(int setWeight, int setValue)
  {
    weight = setWeight;
    value = setValue;
    this->left = NULL;
    this->right = NULL;
  }

  /**
   * HuffmanNode - this is the constructors to set up a parent HuffmanNode
   *   the weight will be set to equal to the combined value of the child nodes.
   *
   * @param left - pointer to the left child node
   *        right - pointer to the right child node
   */
  HuffmanNode::HuffmanNode(HuffmanNode* left, HuffmanNode* right)
  {
  this->left = left;
  this->right = right;
  weight = right->getWeight() + left->getWeight();
  value = -1;
  }
  /**
   * getValue - returns the int value of the node. This is an ascii index to a
   *            character
   */
  int HuffmanNode::getValue()
  {
    return value;
  }

  int HuffmanNode::getWeight()
  {
    return weight;
  }

  /**
   * getLeft - returns a pointer to the left child
   */
  HuffmanNode* HuffmanNode::Left()
  {
    return left;
  }

  /**
   * getRight - returns a pointer to the right child
   */
  HuffmanNode* HuffmanNode::Right()
  {
    return right;
  }
