#ifndef HUFFMANNODE_H
#define HUFFMANNODE_H
#include <iostream>

/**
 * Class HuffmanNode -
 *
 * @author Luke Moran McNamee
 * @version 2016.10.20
 *    "C Level(Complete)"
 */
class HuffmanNode
{
  private:
    int weight;
    int value; // the character held in the node (if no character then this is the value of PSUEDOEOF, 256)

    HuffmanNode* left; // left child
    HuffmanNode* right; // right child
  public:
    /**
     * HuffmanNode - this is the constructors to set up a leaf HuffmanNode
     *   the weight and value will be set to equal to the parameters given.
     *
     * @param setWeight - the weight of the node
     *        setValue - the value of the node
     */
    HuffmanNode(int setWeight, int setValue);

    /**
     * HuffmanNode - this is the constructors to set up a parent HuffmanNode
     *   the weight will be set to equal to the combined value of the child nodes.
     *
     * @param left - pointer to the left child node
     *        right - pointer to the right child node
     */
    HuffmanNode(HuffmanNode* left, HuffmanNode* right);

    /**
     * getValue - returns the int value of the node. This is an ascii index to a
     *            character
     */
    int getValue();

    int getWeight();

    /**
     * getLeft - returns a pointer to the left child
     */
    HuffmanNode* Left();

    /**
     * getRight - returns a pointer to the right child
     */
    HuffmanNode* Right();
  };
  #endif
