#include "HuffmanNode.h"
#include "HeapNode.h"
/**
 * Class HeapNode - A class which is used as a node which is ordered in a heap.
 * this will organize the HuffmanNodes, to which each heapnode points, by the
 * frequency by which they appear the the file which will be read by the
 * ReadFile class.
 *
 * @author Luke Moran McNamee (with contribution from Gary Lewandowski)
 * @version 2016.10.24
 *    "C Level(Complete)"
 */
 using namespace std;

    HeapNode::HeapNode()
    {
      pointerNode = NULL;
    }

    /**
     * value - returns a pointer to the value node
     */
    HuffmanNode* HeapNode::value() const
    {
      return pointerNode;
    }

    /**
     * buildLeaf - given a weight and value, creates a new HuffmanNode with
     * these values
     * @param: value - the value with which to create the leaf
     *         weight - the weight with which to create the leaf
     */
    void HeapNode::buildLeaf(int weight, int value)
    {
      pointerNode = new HuffmanNode(weight, value);
    }

    /**
     * operator< - overloads the < operator to return the greater of value of
     * the two values of the HuffmanNodes which the HeapNode points to. This is
     * the character value stored by this HuffmanNode.
     *
     * @param the right Hand Argument of the operator
     * @returns the greater value stored in the associated HuffmanNode
     */
    bool HeapNode::operator<(const HeapNode& rhs) const //, const HeapNode& lhs)
    {
      return (pointerNode->getWeight() > rhs.value()->getWeight());
    }

     /**
      * buildNode - given two  creates a new HuffmanNode parent with two leaf
      * HuffmanNodes.
      * @param: left - the left leaf node
      *         right - the right leaf node
      */
    void HeapNode::buildNode(HuffmanNode* left, HuffmanNode* right)
    {
      pointerNode = new HuffmanNode(left, right);
    }
