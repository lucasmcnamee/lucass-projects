#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>
#include "CommandWords.h"
#include "Command.h"

/**
 * Player class implements the information of the player
 * including the player's current location and the items player currently carries.
 *
 * It also implements take and drop items methods for player to take available
 * items in the room and to leave them in a certain room.
 *
 * canTakeItem method is a special method to determines whether the player should
 * take the item or not depending on the totalweight of the backpack and the item
 *
 *
 * @author Luke Moran McNamee
 * @version 03-21-2016
 */

using namespace std;

class Parser
{
    private:
      CommandWords commands;
    public:
      Parser();
      Command getCommand();
      void showCommands();
      string trimString(string &toBeTrimmed, int &locOfFirstWord);

};
#endif
