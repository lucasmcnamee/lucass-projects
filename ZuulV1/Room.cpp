#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "Room.h"
#include "Item.h"
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * A "Room" represents one location in the scenery of the game.  It is
 * connected to other rooms via exits.  For each existing exit, the room
 * stores a reference to the neighboring room.
 *
 * A Room can also have an encounter associated with it. These encounters
 * are challenges to complete inside of the room.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

using namespace std;

    /**
     * Room - Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    Room::Room(string description)
    {
        this->description = description;
    }

    /**
     * Define an exit from this room.
     * @param direction The direction of the exit.
     * @param neighbor  The room to which the exit leads.
     */
    void Room::setExit(string direction, Room* neighbor)
    {
        exits[direction] = neighbor;
    }

    /**
     * @return The short description of the room
     * (the one that was defined in the constructor).
     */
    string Room::getShortDescription()
    {
        return description;
    }

    /**
     * Return a description of the room in the form:
     *     You are in the kitchen.
     *     Exits: north west
     * @return A long description of this room
     */
    string Room::getLongDescription()
    {
        return "You are " + description + "\n" + getItemsString() + "\n"+ getExitString();
    }

    /**
     * getExitString - Return a string describing the room's exits, for
     * example, "Exits: north west".
     *
     * @return string: Details of the room's exits.
     */
    string Room::getExitString()
    {
        string returnstring = "Exits:";
        vector<string> keys; // a vector to temporarily store the exits

        for(map<string, Room*>::iterator it = exits.begin(); it != exits.end(); ++it) {
          keys.push_back((*it).first);
        }

        for(int i = 0; i < keys.size(); i++) {
            string exit = keys.at(i);
            returnstring = returnstring + " " + exit;
            // add the new exit to the current exit string
        }
        return returnstring;
    }

    /*
     * Return the room that is reached if we go from this room in direction
     * "direction". If there is no room in that direction, return null.
     * @param direction The exit's direction.
     * @return The room in the given direction.
     */
    Room* Room::getExit(string direction)
    {
      Room* returnRoom = NULL;
      for(map<string, Room*>::iterator it = exits.begin(); it != exits.end(); ++it) {
        if (exits.find(direction) != exits.end())
        {
          returnRoom = exits.at(direction);
        }
      }
      return returnRoom;
    }

    /**
     * add an item into a room
     */
    void Room::addItem(string itemName, Item *item)
    {
        roomInventory.add(itemName, item);
    }

    /**
     * removeItem - remove the item
     *
     * @param itemName  Name of the item
     * @return  The removed item
     */
    Item* Room::removeItem(string itemName)
    {
      Item* returnItem = roomInventory.get(itemName);
      roomInventory.remove(itemName);
        return returnItem;
    }

    /**
     * getItem - get item in the current room
     *
     * @param   itemName Name of the item
     * @return  The item needed
     */
     Item* Room::getItem(string itemName)
    {
        Item* returnMe = NULL;
        if(roomInventory.containsItem(itemName)) {
          returnMe = roomInventory.get(itemName);
        }
        return returnMe;
    }

    /**
     * Checks to see if an item is in the inventory
     *
     * @param The item
     * @returns   True: if the item is in the inventory
     *            False: if the item is not in the inventory
     */
    bool Room::containsItem(string itemName)
    {
      return (roomInventory.containsItem(itemName));
    }

    /**
     * Return a string describing items available in the room.
     * For example, "Items: a bottle of wine"
     *
     * @return Details of the items available in the room
     */
    string Room::getItemsString()
    {
        return roomInventory.getShortItemsString();
    }
