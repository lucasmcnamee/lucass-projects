#ifndef INVENTORY_H
#define INVENTORY_H
/**
 * Inventory works as a way to for both rooms and the player class to store
 *their items
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

#include <map>
#include <string>
#include "Item.h"

using namespace std;

class Inventory
{
  private:
    map<string, Item*> items;

  public:

    /**
     * Checks to see if an item is in the inventory
     *
     * @param The item
     * @returns   True: if the item is in the inventory
     *            False: if the item is not in the inventory
     */
    bool containsItem(string itemName);

    /**
     * Return a string describing items available in the room.
     * For example, "Items: a bottle of wine"
     *
     * @return Details of the items available in the room
     */
     string getShortItemsString();

     /**
      * Return a string describing items available in the room.
      * For example, "Items: a bottle of wine"
      *
      * @return Details of the items available in the room
      */
      string getLongItemsString();

  /**
   * add an item into a room
   */
  void add(string itemName, Item *item);

  /**
   * remove an item
   *
   * @param itemName  Name of the item
   * @return  The removed item
   */
   Item* remove(string itemName);

  /**
   * @param   Name of the item
   * @return  The item needed
   */
   Item* get(string itemName);


};
#endif
