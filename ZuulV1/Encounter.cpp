#include "Room.h"
#include "Item.h"
#include "Command.h"
#include "Encounter.h"
#include <string>
/**
 * Class Encounter is used to make events and challenges in each room.
 * If the room has no encounter then the room's description will show
 *
 *         challegeIntro - description of the challenge, "at the other end
 *                of the room you see a locked door"
 *         lethalChallenge - true the challenge will kill you
 *                false player will simply remain in the room
 *         success - the message to play if the player completes the
 *                challenge, "The key fits smoothly into the lock and you hear
 *                a click as the door unlocks"
 *         failure - the message to be played if the player does not
 *                complete the challenge, "not matter how hard you fiddle,
 *                push, wiggle, and bang the " + item + "against the door.
 *                It refuses to give way."
 *         winningCommand - the command needed to beat the challenge, "Use
 *         keys"
 *
 * @author Luke Moran McNamee
 * @version 2016.10.07
 */

 using namespace std;

Encounter::Encounter(bool lethalChallenge, string challengeIntro, string success, string failure, Item* winningItem)
{
  this->lethalChallenge = lethalChallenge;
  this->challengeIntro = challengeIntro;
  this->success = success;
  this->failure = failure;
  this->winningItem = winningItem;
  completed = false;
}

  /**
   * getChallengeIntro - returns the introductions of the challenge as a string
   */
  string Encounter::getChallengeIntro()
  {
     return challengeIntro;
  }

  /**
   * hasBeenCompleted - returns true if the challenge has already been completed
   */
  bool Encounter::hasBeenCompleted()
  {
    return completed;
  }

  /**
   * getSuccessString - returns the message to be played if the player
   * successfully completes the challenge
   */
  string Encounter::getSuccessString()
  {
     completed = true;
     return success;
  }

  /**
   * getFailureString - returns the message to be played if the player
   * unsuccessfully completes the challenge
   */
  string Encounter::getFailureString()
  {
     return failure;
  }

  /**
   * winEncounter - returns true if the command is the correct command to win
   * the challenge
   */
  string Encounter::winEncounter()
  {
     return winningItem->getName();
  }
