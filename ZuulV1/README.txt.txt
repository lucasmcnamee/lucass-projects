World of Zuul v1.0

Author: Luke Moran McNamee & Mike Lanter

Description: This Project is a text based adventure game created originally by Michael K�lling and David 
	J. Barnes in Java and converted into C++ and modified by me! When you play the game you will start 
	in one room and be able to move inbetween a series of rooms using the "go" command. Use the "look" 
	command to see what items a room has and then use "take" to pick up the item. Currently only one
	room has an "Encounter" which will require the user to complete a small challenge using the "use"
	command with an item in order to be able to advance to the next room. 
	Complete the game by finding the GoldStar!
	
How to use: Open the project folder and click zuul.exe and the game will start! 

Version (1.1.0) Ideas:
	-Parser: parser can parse a third and fourth word. This would allow "look at [Item]" to get more 
		 information about the item. Also it would allow for the "use" to become slightly more 
		 challenging. "Use" + Item could be used to use a consumable like a health potion and for 
 		 encounters, "use" + Item + (on) + object, ex: "use keys on door."
	-LevelStore: add a second level, add lethal encounters.
	-Inventory: (this could be critical) add a maxweight to an inventory. With this we could make the  
		 biggest challenge in the game be managing ones inventory. So a player has to plan out what
		 he/she wants to take. The important thing here is that if we impliment this we need to
		 have critical items and optional items. Optional items will help the player, map. Whereas
		 Critical items will be required, keys. Balancing these two things may be difficult.
	-Game(Encounter): one parameter of encounter is wheather or not the encounter is a lethal 
		 challenge. This should be implimented. When a player fails a lethal challenge they should
		 restart the game or level.


	-(TBD) Impliment Interfaces or (preferably) inheritence to remove code redundancies in classes like 
		 room and player for accessing their inventories. 

  To be implimented later:
	-Game: 	 Difficulty settings, when you fail you restart game instead of level, less health etc.
	-Player: HP, persona stats
	-Enemies: Very simple enemies just a different kind of encounter
	
Have Fun!