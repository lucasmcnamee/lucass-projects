#include <string>
#include <iostream>
#include "Item.h"
#include <stdio.h>
#include <stdlib.h>
/**
 * Item class contains name, weight and description of an item
 *
 * @author Luke Moran McNamee
 * @version 2016.10.6
 */
using namespace std;
    /**
     * Item create an item descibed "description."
     *
     * @param    The item's name, description, and weight
     */
    Item::Item(string name, string description)
    {
        this->name = name;
        this->description = description;
    }

    /**
     *  get the name of the item
     *
     * @return The name of the item
     */
    string Item::getName()
    {
        return name;
    }

    /**
     *  get the description of the item
     *
     * @return The item's description
     */
    string Item::getDescription()
    {
        return description;
    }
