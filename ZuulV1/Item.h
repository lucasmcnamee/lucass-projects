#ifndef ITEM_H
#define ITEM_H
/**
 * Item class contains name, weight and description of an item
 *
 * @author Luke Moran McNamee
 * @version 2016.10.6
 */
 #include <string>
 #include <iostream>
 #include "Item.h"
 #include <stdio.h>
 #include <stdlib.h>
using namespace std;

class Item
{
  private:
    string name;
    string description;
  public:
    /**
     * Item create an item descibed with a name, description, and weight
     *
     * @param    The item's name, description, and weight
     */
    Item(string name, string description);

    /**
     * @return A string description of the item and its weight
     */
    string toString();

    /**
     *  get the name of the item
     *
     * @return The name of the item
     */
    string getName();

    /**
     *  get the description of the item
     *
     * @return The item's description
     */
    string getDescription();
  };
  #endif
