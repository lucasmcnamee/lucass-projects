#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <map>
#include "Item.h"
#include "Room.h"
#include "Inventory.h"

/**
 * Player class implements the information of the player
 * including the player's current location and the items player currently carries.
 *
 * It also implements take and drop items methods for player to take available
 * items in the room and to leave them in a certain room.
 *
 * @author Luke Moran McNamee
 * @version 2016.10.06
 */

using namespace std;

class Player
{
    private:
      Room* currentRoom;
      // the player's current location
      Inventory pack;
      // the collection of items that the player has
    public:

      /**
       * newRoom - changes the players location
       *
       * @param  The room in which the player enters
       */
      void newRoom(Room* room);

      /**
       * containsItem - Checks to see if an item is in the inventory
       *
       * @param string: item name
       * @returns   True: if the item is in the inventory
       *            False: if the item is not in the inventory
       */
      bool containsItem(string itemName);

      /**
       * getCurrentRoom - returns The player's current room
       */
      Room* getCurrentRoom();

      /**
       * Return a string describing items available in the to the player.
       * For example, "Items: a bottle of wine"
       *
       * @return Details of the items available in the room
       */
      string getItemsString();

      /**
       * takeItem - take the specified item in the current room
       * IF THE ITEM PROVIDED IS NOT IN THE ROOM THIS WILL CAUSE ERRORS (Segmentation Fault)
       *
       * @param    string: The name of the item
       * @return   Item*: the item that is picked up
       */
      Item* takeItem(string itemName);

      /**
       * dropItem - leave the specified item in the current room
       *
       * @param   itemName The name of the item
       * @return  The item is dropped
       */
      Item* dropItem(string itemName);
};
#endif
