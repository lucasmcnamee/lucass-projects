#include <string>
#include <vector>
#include "Player.h"
#include "Room.h"
#include "Item.h"
#include <iostream>
/**
 * Player class implements the information of the player
 * including the player's current location and the items player currently carries.
 *
 * It also implements take and drop items methods for player to take available
 * items in the room and to leave them in a certain room.
 *
 * @author Luke Moran McNamee
 * @version 2016.10.06
 */

using namespace std;

    /**
     * newRoom - changes the players location
     *
     * @param  The room in which the player enters
     */
    void Player::newRoom(Room* room)
    {
        currentRoom = room;
    }

    string Player::getItemsString()
    {
        return pack.getLongItemsString();
    }

    /**
     * containsItem - Checks to see if an item is in the inventory
     *
     * @param    string: item name
     * @return   true: if the item is in the inventory
     *           false: if the item is not in the inventory
     */
    bool Player::containsItem(string itemName)
    {
      return (pack.containsItem(itemName));
    }

    /**
     * getCurrentRoom - returns The player's current room
     */
    Room* Player::getCurrentRoom()
    {
        return currentRoom;
    }

    /**
     * takeItem - take the specified item in the current room
     * IF THE ITEM PROVIDED IS NOT IN THE ROOM THIS WILL CAUSE ERRORS (Segmentation Fault)
     *
     * @param    string: The name of the item
     * @return   Item*: the item that is picked up
     */
    Item* Player::takeItem(string itemName)
    {
        Item* item = currentRoom->removeItem(itemName);
        // WARNING! will cause errors if the item is not in the room
        pack.add(itemName, item);
      return item;
    }

      /**
       *  dropItem - leave the speicified item in the current room
       *
       * @param   The name of the item
       * @return  The item is dropped
       */
    Item* Player::dropItem(string itemName)
    {
      return (pack.remove(itemName));
    }
