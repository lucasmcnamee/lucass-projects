#include <iostream>
#include <string>
#include "Parser.h"
#include "Room.h"
#include "Game.h"
#include "Item.h"
/**
 *  This class is the main class of the "World of Zuul" application.
 *  "World of Zuul" is a very simple, text based adventure game.  Users
 *  can walk around some scenery, pick up items, and use those items to complete
 *  challenges!
 *
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 *
 *  This main class creates and initialises all the others: creates the parser
 *  and starts the game. However the LevelStore holds the information for the
 *  rooms and encounters It also evaluates and
 *  executes the commands that the parser returns.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

using namespace std;

    /**
     * Create the game and initialise its internal map by accessing the LevelStore class
     */
    Game::Game()
    {
      playingEncounter = false;
      wantToQuit = false;
      currentRoom = levels.createLevelOne();
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    void Game::play()
    {
        player.newRoom(currentRoom);

        printWelcome();
        cout << endl;


        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        bool finished = false;
        while (!wantToQuit && !finished) {
          if(levels.mapContains(player.getCurrentRoom()) && !playingEncounter && !levels.getEncounter(player.getCurrentRoom())->hasBeenCompleted()) {
            playEncounter(player.getCurrentRoom());
          }
            Command command = parser.getCommand();
            cout << endl;
            finished = processCommand(command);
        }
        cout << "Thank you for playing.  Good bye." << endl;
    }

    /**
     * Print out the opening message for the player.
     */
    void Game::printWelcome()
    {
        cout << "..............................................................." << endl;
        cout << "Welcome to the World of Zuul!" << endl;
        cout << "World of Zuul is a new, incredibly boring adventure game." << endl;
        cout << "Type 'help' if you need help." << endl;
        cout << "..............................................................." << endl;
        cout << player.getCurrentRoom()->getLongDescription() << endl;
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    bool Game::processCommand(Command command)
    {
      if(wantToQuit == true) {
        return true;
      }
        //bool wantToQuitLoop = false;

        if(command.isUnknown()) {
            cout << "I don't know what you mean..." << endl;
            return false;
        }

        string commandWord = command.getCommandWord();
        if (commandWord == "help") {
            printHelp();
        }
        else if (commandWord == "go") {
            goRoom(command);
        }
        else if (commandWord == "take") {
            take(command);
        }
        else if (commandWord == "drop") {
            drop(command);
        }
        else if (commandWord == "use") {
            use(command);
        }
        else if (commandWord == "inventory") {
            inventory();
        }
        else if (commandWord == "look") {
            look();
        }
        else if (commandWord == "quit") {
            wantToQuit = quit();
        }
        else if ((commandWord == "back") || (commandWord == "return")) {
            goBack();  //return to previous room
        }
        return wantToQuit;
    }

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the
     * command words.
     */
    void Game::printHelp()
    {
        cout << "You are lost. You are alone. You wander" << endl;
        cout << "around at the university." << endl;
        cout << endl;
        cout << "Your command words are:" << endl;
        parser.showCommands();
    }

    /**
     * Pick up an item and place it into the players inventory
     */
     void Game::take(Command command)
     {
       if(!command.hasSecondWord()) {
           // if there is no second word, we don't know what to take...
           cout << "Take what?" << endl;
           return;
       }

       string itemName = command.getSecondWord();

       // Try to pick up the item.
       if (!player.getCurrentRoom()->containsItem(itemName)) {
         cout << "There is no " + itemName + " in this room!" << endl;
       } else {
            Item* item = player.takeItem(itemName);
            if(itemName == "goldstar") {
              cout << "You hear a rumbling sound, you stare amazed as a portal opens up in front of you." << endl;
              cout << "A disembodied voice seeming to come from inside the portal itself mutters a feeble plea for help." << endl;
              cout << "You step inside the portal." << endl << "To be Continued... Next time on Dragonball Z" << endl;
              cout << endl << "Congratuations! you have completed this version of the game!" << endl << "Type quit to leave" << endl;
        //      wantToQuit = true;
            }
            else {
            cout << "You pick up " + item->getDescription() << endl;
              }
         }
     }

     /**
      * Drops an item and leaves it in the room
      *
      * @return
      */
     void Game::drop(Command command)
     {
       if(!command.hasSecondWord()) {
           // if there is no second word, we don't know what to drop...
           cout << "Drop what?" << endl;
           return;
       }

       // Try to drop the item.
       string itemName = command.getSecondWord();
       if (!player.containsItem(itemName)) {
           cout << "There is no " + itemName + " in your inventory!" << endl;
       } else {
           Item* item = player.dropItem(itemName); // remove the item from the player's inventory
           player.getCurrentRoom()->addItem(itemName, item); // add the item to the room's inventory
           cout << "You drop the " + itemName << endl;
         }
     }

    /**
     * Displays all the items in the room
     */
     void Game::look()
     {
       cout << player.getCurrentRoom()->getItemsString() << endl;
     }

     /**
      * Prints a string describing items available in the to the player.
      * For example, "Items: a bottle of wine"
      */
     void Game::inventory()
     {
       cout << player.getItemsString() << endl;
     }

     /**
      * back - takes the player back to the previous visited room
      */
     void Game::goBack()
     {
        if (previousRooms.empty()) {
            cout << "There's no more previous room" << endl;
        }  else {
            Room* previousRoom = previousRooms.top();
            previousRooms.pop();
            player.newRoom(previousRoom);
            cout << player.getCurrentRoom()->getLongDescription() << endl;
            playingEncounter = false;
        }
    }

     /**
      * Try to in to one direction. If there is an exit, enter the new
      * room, otherwise print an error message.
      */
     void Game::goRoom(Command command)
     {
         if(!command.hasSecondWord()) {
           // if there is no second word, we don't know where to go...
           cout << "Go where?" << endl;
           return;
         }

         string direction = command.getSecondWord();

         // Try to leave current room.
         Room *nextRoom = player.getCurrentRoom()->getExit(direction);
         if(direction == "back") {
           goBack();
           playingEncounter = false;
         }
         else if (nextRoom == NULL) {
           cout << "There is no door!" << endl;
         }
         else if(!playingEncounter || nextRoom->getShortDescription() == previousRooms.top()->getShortDescription()) {
                 previousRooms.push(player.getCurrentRoom());
                 player.newRoom(nextRoom);
                 cout << nextRoom->getLongDescription() << endl;
                 playingEncounter = false;
         } else {
             cout << levels.getEncounter(player.getCurrentRoom())->getFailureString() << endl;
             cout << "you must figure this challenge out or go back." << endl;
           }
     }

     /**
      * Begins a subroutine of the main game loop which begins a game encounter
      */
     void Game::playEncounter(Room* roomName)
     {
       Encounter* encounter = levels.getEncounter(roomName);
       playingEncounter = true; // prevents the encounter's intro message from printing each command loop
       cout << encounter->getChallengeIntro() << endl;
       cout << "What do you do?" << endl;
     }

     /**
      * use - this class is used by the encounter class to complete the encounter
      */
     void Game::use(Command command)
     {
         if(command.getCommandWord() == "use") {
           levels.hasEncounter(player.getCurrentRoom());
           Encounter* encounter = levels.getEncounter(player.getCurrentRoom());

           if(!command.hasSecondWord()) { // if the player does not speicify what to use
             cout << "use what item?";
           }
           else if(!player.containsItem(command.getSecondWord())) { // if the player does not have the item they tried to use
             cout << "you do not have a " + command.getSecondWord() + " in your pack!" << endl;
           }
             else if(encounter->winEncounter() == command.getSecondWord()) { // if the player wins
               cout << encounter->getSuccessString() << endl;
               cout << player.getCurrentRoom()->getLongDescription() << endl;
               playingEncounter = false; // lets the game know to not keep the encounter running repeatedly

             } else { // else the player has failed
               cout << encounter->getFailureString() << endl;
              }
         }
     }


     /**
      * "Quit" was entered. Check the rest of the command to see
      * whether we really quit the game.
      * @return true, if this command quits the game, false otherwise.
      */
     bool Game::quit()
     {
       return true;  // signal that we want to quit
     }
