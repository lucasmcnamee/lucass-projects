#ifndef ENCOUNTER_H
#define ENCOUNTER_H
/**
 * Class Encounter is used to make events and challenges in each room.
 * If the room has no encounter then the room's description will show
 *
 * @author Luke Moran McNamee
 * @version 2016.10.07
 */
// #include "Room.h"
 #include "Item.h"
 #include "Command.h"
 #include "Room.h"

 using namespace std;

 class Encounter
 {
  private:
    bool completed, lethalChallenge;
    string challengeIntro, success, failure;
    // if there is a room that is unlocked by completing this challege this will be the direction
    Item* winningItem;

  public:
    /**
     * @param: challegeIntro - description of the challenge, "at the other end
     *                of the room you see a locked door"
     *         lethalChallenge - true the challenge will kill you
     *                false player will simply remain in the room
     *         success - the message to play if the player completes the
     *                challenge, "The key fits smoothly into the lock and hear
     *                a click as the door unlocks"
     *         failure - the message to be played if the player does not
     *                complete the challenge, "not matter how hard you fiddle,
     *                push, wiggle, and bang the " + item + "against the door.
     *                It refuses to give way."
     *         winningCommand - the command needed to beat the challenge, "Use
     *         keys"
     */
    Encounter(bool lethalChallenge, string challengeIntro, string success, string failure, Item* winningItem);

    /**
     * getChallengeIntro - returns the introductions of the challenge as a string?
     */
    string getChallengeIntro();

    /**
     * getSuccessString - returns the message to be played if the player
     * successfully completes the challenge
     */
    string getSuccessString();

    /**
     * getFailureString - returns the message to be played if the player
     * unsuccessfully completes the challenge
     */
    string getFailureString();

    /**
     * winEncounter - returns true if the command is the correct command to win
     * the challenge
     */
    string winEncounter();

    /**
     * hasBeenCompleted - returns true if the challenge has already been completed
     */
     bool hasBeenCompleted();


  };
  #endif
