#include <iostream>
#include <string>
#include "LevelStore.h"
#include "Item.h"
#include "Encounter.h"
/**
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * This class stores the information about the levels. This means it stores all
 * of the information needed to instantiate any rooms, items, and encounters for
 * the game. The class only have accessor functions and functions to instantiate
 * new levels, although as of now there is only one level.
 *
 * @author  Luke Moran McNamee
 * @version 0.2.1 - 2016.10.10
 */
using namespace std;

    /**
     * createLevelOne() - creates the first level of the game. There is only one level in this version
     * Instantiates all rooms, encounters, and items and stores them for later retrieval
     * @return the Room* in which level one begins, a.k.a outside.
     */
    Room* LevelStore::createLevelOne()
    {
      Room *outside, *theater, *pub, *lab, *office, *secretRoom;

      // create the rooms
      outside = new Room("outside the main entrance of the university");
      theater = new Room("in a lecture theater");
      pub = new Room("in the campus pub");
      lab = new Room("in a computing lab");
      office = new Room("in the computing admin office");
      secretRoom = new Room("a secret room hidden behind the pub");

      // add the rooms to roomsMap
      roomsMap["outside"] = outside;
      roomsMap["theater"] = theater;
      roomsMap["pub"] = pub;
      roomsMap["lab"] = lab;
      roomsMap["office"] = office;
      roomsMap["secret room"] = secretRoom;

      // initialise room exits
      outside->setExit("east", theater);
      outside->setExit("south", lab);
      outside->setExit("west", pub);

      theater->setExit("west", outside);

      pub->setExit("east", outside);
      pub->setExit("west", secretRoom);

      secretRoom->setExit("east", pub);

      lab->setExit("north", outside);
      lab->setExit("east", office);

      office->setExit("west", lab);

      // initialise room items
      Item* keys = new Item("keys", "a pair of old keys for the university but the label on the key has been worn away");
      outside->addItem("pen", new Item("pen", "a classic, purple pen probably used for grading bad projects"));

      theater->addItem("map", new Item("map", "a map which seems to show that there is a hidden room west of the pub"));
      pub->addItem("wine", new Item("wine", "a dusty bottle of wine, it looks expensive though"));
      lab->addItem("book", new Item("book", "Object First with Java book, What could this mean?"));
      office->addItem("keys", keys);
      secretRoom->addItem("goldstar", new Item("goldstar", "Congrats! I hope you feel proud!!!!!!! <3"));

      // initialise room encounters (Note only rooms that will have an encounter are added to the map)
      string s1, s2, s3;
      s1 = "at the other end of the room you see a locked door";
      s2 = "The key fits smoothly into the lock and hear a click as the door unlocks";
      s3 = "no matter how hard you fiddle, push, wiggle, and bang the against the door. It refuses to give way.";

      // instantiate encounters and add them to the encounterMap
      Encounter* lockedDoor = new Encounter(false, s1, s2, s3, keys);
      encounterMap[pub] = lockedDoor;

      return outside;  // returns the room in which the level will begin.
    }

    /**
     * hasEncounter - test if a given @param room has an encounter
     * @return    true: room has an encounter
     *            false: room does not have an encounter
     */
    bool LevelStore::hasEncounter(Room* roomName)
    {
          bool returnMe = false;
          for(map<Room*, Encounter*>::iterator it = encounterMap.begin(); it != encounterMap.end(); ++it) {
           if(roomName == (*it).first) {
               returnMe = true;
           }
          }
          return returnMe;
    }

    /**
     * getEncounter - returns the encounter associated with the @param room
     */
    Encounter* LevelStore::getEncounter(Room* roomName)
    {
      return encounterMap.at(roomName);
    }

    /**
     * mapContains
     * @return     true: the @param Room has an Encounter in encounterMap
     *             false: the @param room does not have an encounter
     */
    bool LevelStore::mapContains(Room* room)
      {
        bool returnMe = false;
        for(map<Room*, Encounter*>::iterator it = encounterMap.begin(); it != encounterMap.end(); ++it) {
         if(room == (*it).first) {
             returnMe = true;
         }
        }
        return returnMe;
      }
