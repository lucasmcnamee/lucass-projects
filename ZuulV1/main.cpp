/**
* Runs the zuul game and the zuul tets
*
* @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
* @version 2016.10.03
**/
#include <iostream>
#include "Game.h"

using namespace std;

int main()
{
			Game theGame;
			theGame.play();
}
// To compile use one of the next two lines 
// $ g++ main.cpp command.cpp CommandWords.cpp parser.cpp room.cpp game.cpp inventory.cpp encounter.cpp player.cpp -o main
// $ mingw32-make
