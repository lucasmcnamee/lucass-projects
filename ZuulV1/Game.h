#ifndef GAME_H
#define GAME_H
/**
 *  This class is the main class of the "World of Zuul" application.
 *  "World of Zuul" is a very simple, text based adventure game.  Users
 *  can walk around some scenery. That's all. It should really be extended
 *  to make it more interesting!
 *
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 *
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

#include "Parser.h"
#include "Room.h"
#include "player.h"
#include "Encounter.h"
#include "LevelStore.h"

#include <map>
#include <stack>


using namespace std;

class Game
{
  private:
    Room *currentRoom;
    Parser parser;
    Player player;
    LevelStore levels;
    bool playingEncounter;
    stack<Room*> previousRooms;
    bool wantToQuit;
    map<Room*, Encounter*>encounterMap;

    /**
     * Create all the rooms and link their exits together.
     */
    void createRooms();

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    bool processCommand(Command command);

    /**
     * @returns true if the Room has an Encounter in encounterMap
     */
    bool mapContains(Room* room);

    /**
     * Print out the opening message for the player.
     */
    void printWelcome();

    /**
     * Begins a subroutine of the main game loop which begins a game encounter
     */
    void playEncounter(Room* roomName);

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the
     * command words.
     */

    void printHelp();

    /**
     * Drops an item and leaves it in the room
     *
     * @return
     */
    void drop(Command command);

    /**
     * Pick up an item and place it into the players inventory
     */

    void take(Command command);

    /**
     * Displays all the items in the room
     */
    void look();

    /**
     * Prints a string describing items available in the to the player.
     * For example, "Items: a bottle of wine"
     */
    void inventory();

    /**
     * back - takes the player back to the previous visited room
     */
    void goBack();

    /**
     * Try to in to one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    void goRoom(Command command);

    /**
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    bool quit();

    void use(Command command);

  public:
    map<string, Room*> findRooms;

    /**
     * Create the game and initialise its internal map.
     */
    Game();

    /**
     *  Main play routine.  Loops until end of play.
     */
    void play();


};

 #endif
