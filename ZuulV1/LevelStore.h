#ifndef LEVELSTORE_H
#define LEVELSTORE_H

#include <string>
#include <map>

#include "Encounter.h"
#include "Room.h"
#include "Player.h"
/**
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * This class stores the information about the levels. This means it stores all
 * of the information needed to instantiate any rooms, items, and encounters for
 * the game. The class only have accessor functions and functions to instantiate
 * new levels, although as of now there is only one level.
 *
 * @author  Luke Moran McNamee
 * @version 0.2.1 - 2016.10.10
 */
using namespace std;

class LevelStore
{
    private:
      map<Room*, Encounter*> encounterMap;
      //stores the room and its associated encounter
      map<string, Room*> roomsMap;
      //helps to find the rooms (may be deleted as it is not used currently)
      Player player;

    public:
      /**
       * createLevelOne() - creates the first level of the game. There is only one level in this version
       * Instantiates all rooms, encounters, and items and stores them for later retrieval
       * @return the Room* in which level one begins, a.k.a outside.
       */
      Room* createLevelOne();

      /**
       * hasEncounter - test if a given @param room has an encounter
       * @return    true: room has an encounter
       *            false: room does not have an encounter
       */
      bool hasEncounter(Room* roomName);

      /**
       * getEncounter - returns the encounter associated with the @param room
       */
      Encounter* getEncounter(Room* roomName);

      /**
       * mapContains
       * @return     true: the @param Room has an Encounter in encounterMap
       *             false: the @param room does not have an encounter
       */
      bool mapContains(Room* room);
};
#endif
