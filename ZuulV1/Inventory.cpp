#include <string>
#include <iostream>
#include "Inventory.h"
#include <map>
/**
 * Inventory works as a way to for both rooms and the player class to store
 *their items
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */
using namespace std;

/**
 * Return a string describing items available in the room.
 * For example, "In this room you can see: wine"
 *
 * @return Details of the items available in the room
 */
 string Inventory::getShortItemsString()
 {
     string returnstring = "In this room you can see:";

     for(map<string, Item*>::iterator it = items.begin(); it != items.end(); ++it) {
       returnstring = returnstring + " " + (*it).first;
     }
     return returnstring;
 }

 /**
  * Return a string describing items available in the room.
  * For example, "Items: a bottle of wine"
  *
  * @return Details of the items available in the room
  */
  string Inventory::getLongItemsString()
  {
      string returnstring = "Items:";

      for(map<string, Item*>::iterator it = items.begin(); it != items.end(); ++it) {
        returnstring = returnstring + " " + (*it).second->getDescription();
      }
      return returnstring;
  }

/**
 * Checks to see if an item is in the inventory
 *
 * @param The item
 * @returns   True: if the item is in the inventory
 *            False: if the item is not in the inventory
 */
bool Inventory::containsItem(string itemName)
  {
    bool returnMe = false;
    for(map<string, Item*>::iterator it = items.begin(); it != items.end(); ++it) {
     if(itemName == (*it).first) {
         returnMe = true;
     }
    }
    return returnMe;
  }
    /**
     * Item create an item descibed "description."
     *
     * @param    The item's name, description, and weight
     */
/**
 * add an item into a room
 */
void Inventory::add(string itemName, Item *item)
{
    items[itemName] = item;
}

/**
 * remove an item
 *
 * @param itemName  Name of the item
 * @return  The removed item
 */
Item* Inventory::remove(string itemName)
{
  Item* returnItem = items.at(itemName);
  items.erase(itemName);
    return returnItem;
}

/**
 * @param   Name of the item
 * @return  The item needed
 */
 Item* Inventory::get(string itemName)
{
    return items.at(itemName);
}
