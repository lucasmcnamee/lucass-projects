/**
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */
 #include <iostream>
 #include <string>
 #include "CommandWords.h"

using namespace std;
    /**
     * Constructor - initialise the command words.
     */
    CommandWords::CommandWords()
    {
      validCommands.push_back("go");
      validCommands.push_back("help");
      validCommands.push_back("quit");
      validCommands.push_back("take");
      validCommands.push_back("look");
      validCommands.push_back("drop");
    }

    bool CommandWords::isCommand(string aString)
    {
        for(int i = 0; i < validCommands.size(); i++) {
            if(validCommands.at(i) == aString)
                return true;
        }
        // if we get here, the string was not found in the commands
          return false;
    }

    void CommandWords::showAll()
    {
        for(int i = 0; i < validCommands.size(); i++) {
            cout << validCommands.at(i) << endl;
        }
    }
