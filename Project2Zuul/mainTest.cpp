/**
* Runs the zuul tets
*
* @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
* @version 2016.10.03
**/
#include <iostream>
#include <string>
#include "CommandWords.h"
#include "Command.h"
#include "Parser.h"
#include "Room.h"
#include "Game.h"

using namespace std;

int main()
{
		 cout << "Welcome to my project! The tests for each class will run and then the game will begin!" << endl << endl;
		CommandWords *commandword = new CommandWords(); //for testing

		string commandWordInvalid = "hello";
		string commandWordValid = "help";

		cout << "CommandWords tests!" << endl;
		cout << "showAll(): " << endl;
		commandword->showAll();

		cout << "If the string 'hello' is a commandWord a 1 will print: " << commandword->isCommand(commandWordInvalid) << endl;
		cout << "If the string 'help' is a commandWord a 1 will print: " << commandword->isCommand(commandWordValid) << endl;

		cout << endl;
		cout << "Class Command: " << endl << endl;

		string wordOne = "wordOne";
		cout << "wordOne = 'wordOne'" << endl;

		string wordTwo = "wordTwo";
		cout << "wordTwo = 'wordTwo'" << endl << endl;
		//these two strings represent valid strings which will return a 1

		string wordOneVoid;
		string wordTwoVoid;
		//these two strings represent invalid strings which will return a 0

		Command *commandTest = new Command(wordOne, wordTwo);

		cout << "getCommandWord will return the wordOne of the new command: " << commandTest->getCommandWord() << endl;
		cout << "getSecondWord will return the wordOne of the new command: " << commandTest->getSecondWord() << endl << endl;

		cout << "if commandWord is unknown returns 1, this string is valid: " << commandTest->isUnknown() << endl;
		cout << "if secondWord has second word returns 1, this string is valid: " <<commandTest->hasSecondWord();

		Command *commandTest1 = new Command(wordOneVoid, wordTwoVoid);

		cout << commandTest1->getCommandWord() << endl;
		cout << commandTest1->getSecondWord() << endl << endl;

		cout << "if commandWord is unknown returns 1, this string is not valid: " << commandTest1->isUnknown() << endl;
		cout << "if secondWord has second word returns 1, this string is not valid: " << commandTest1->hasSecondWord() << endl;
		cout << endl;

		cout << "Class Parser:" << endl;
		cout << "Please input a string of two words with any number of spaces to separate them, all spaces before or inbetween words are discarded. The two words will be printed." << endl;

		Parser *parser = new Parser();
		Command parserTest = (*parser).getCommand();

		cout << endl << "If the commandword you typed is a valid commandWord it will print here: " << parserTest.getCommandWord() << endl;
		cout << "Here is the secondWord you typed: " << parserTest.getSecondWord() << endl << endl;

		cout << "Class Room:" << endl << endl;

		Room *room1 = new Room("in room1");
		Room *room2 = new Room("in room2");
		room1->setExit("East", room2);
		room2->setExit("West", room1);
		//cout << room1->getShortDescription() << endl;
		cout << room1->getLongDescription() << endl;
		cout << room2->getLongDescription() << endl << endl;

		cout << "Class Game cannot be tested as all of its functions are private. To test it play the game!" << endl << endl;

		//Player player = new Player();

}
// to compile use the next line
// g++ mainTest.cpp command.cpp CommandWords.cpp parser.cpp room.cpp game.cpp Item.cpp
