#include <string>
#include <vector>
#include "Player.h"
#include "Room.h"
#include "Item.h"
#include <iostream>
/**
 * Player class implements the information of the player
 * including the player's current location and the items player currently carries.
 *
 * It also implements take and drop items methods for player to take available
 * items in the room and to leave them in a certain room.
 *
 * canTakeItem method is a special method to determines whether the player should
 * take the item or not depending on the totalweight of the backpack and the item
 *
 *
 * @author Luke Moran McNamee
 * @version 2016.10.06
 */
using namespace std;

    /**
     * changes the players location
     *
     * @param  The room in which the player enters
     */
    void Player::newRoom(Room* room)
    {
        currentRoom = room;
    }

    /**
     * Checks to see if an item is in the inventory
     *
     * @param The item
     * @returns   True: if the item is in the inventory
     *            False: if the item is not in the inventory
     */
    bool Player::containsItem(string itemName)
    {
      return (pack.containsItem(itemName));
    }

    /**
     * @return The current room
     */
    Room* Player::getCurrentRoom()
    {
        return currentRoom;
    }

    /**
     * take the specified item in the current room
     *
     * @param    The name of the item
     * @return   The item that is picked up
     */
    Item* Player::takeItem(string itemName)
    {
      Item *item = NULL;
      if (currentRoom->containsItem(itemName)) {
          item = currentRoom->removeItem(itemName);
          pack.add(itemName, item);
        }
      return item;
    }

      /**
       *  leave the speicified item in the current room
       *
       * @param   The name of the item
       * @return  The item is dropped
       */
    Item* Player::dropItem(string itemName)
    {
      return (pack.remove(itemName));
    }
