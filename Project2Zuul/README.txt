Project 2: Zuul

Author: Lucas McNamee

Description: This Project is a text based adventure game created originally by Michael K�lling and David 
	J. Barnes in Java and converted into C++ and modified by me! When you play the game you will start 
	in one room and be able to move inbetween a series of rooms using the "go" command.
	
How to use: Open the project folder and click zuul.exe and the game will start! If instead you want to 
	run through the tests I have made to properly assess if each class is working correctly, click
	zuulTest.exe. 
	
Assignment: 
	-Code compiles 
	-Command, CommandWords, Parser, Room and Game are converted to C++.
	-Conversion of the files to C++ includes .h files for each.
	-The main function is used to test each of these modules and convincingly demonstrates they work.
	-The code is appropriately documented. THAT INCLUDES YOUR NAME ON EVERY FILE.
	-Game runs successfully and we can get into and out of each room.
	-All code is appropriately documented.
Have Fun!