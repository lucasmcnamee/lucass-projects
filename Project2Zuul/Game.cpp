#include <iostream>
#include <string>
#include "Parser.h"
#include "Room.h"
#include "Game.h"
#include "Item.h"
/**
 *  This class is the main class of the "World of Zuul" application.
 *  "World of Zuul" is a very simple, text based adventure game.  Users
 *  can walk around some scenery. That's all. It should really be extended
 *  to make it more interesting!
 *
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 *
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 *
 * @author  Michael K�lling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

using namespace std;

    /**
     * Create the game and initialise its internal map.
     */
    Game::Game()
    {
        createRooms();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    void Game::createRooms()
    {
        Room *outside, *theater, *pub, *lab, *office;

        // create the rooms
        outside = new Room("outside the main entrance of the university");
        theater = new Room("in a lecture theater");
        pub = new Room("in the campus pub");
        lab = new Room("in a computing lab");
        office = new Room("in the computing admin office");

        // initialise room exits
        outside->setExit("east", theater);
        outside->setExit("south", lab);
        outside->setExit("west", pub);

        theater->setExit("west", outside);

        pub->setExit("east", outside);

        lab->setExit("north", outside);
        lab->setExit("east", office);

        office->setExit("west", lab);

        outside->addItem("keys", new Item("keys", "a pair of old keys on the ground"));
        theater->addItem("glasses", new Item("glasses", "a pair of cool glasses next to the door"));
        pub->addItem("wine", new Item("wine", "a bottle of wine left on the counter"));
        lab->addItem("book", new Item("book", "Object First with Java book in the middle of the room"));
        office->addItem("pen", new Item("pen", "a classic, purple pen on the table"));

        player.newRoom(outside);  // start game outside
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    void Game::play()
    {
        printWelcome();
        cout << endl;

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        bool finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        cout << "Thank you for playing.  Good bye." << endl;
    }

    /**
     * Print out the opening message for the player.
     */
    void Game::printWelcome()
    {
        cout << endl;
        cout << "Welcome to the World of Zuul!" << endl;
        cout << "World of Zuul is a new, incredibly boring adventure game." << endl;
        cout << "Type 'help' if you need help." << endl;
        cout << endl;
        cout << player.getCurrentRoom()->getLongDescription() << endl;
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    bool Game::processCommand(Command command)
    {
        bool wantToQuit = false;

        if(command.isUnknown()) {
            cout << "I don't know what you mean..." << endl << endl;
            return false;
        }

        string commandWord = command.getCommandWord();
        if (commandWord == "help") {
            printHelp();
        }
        else if (commandWord == "go") {
            goRoom(command);
        }
        else if (commandWord == "take") {
            take(command);
        }
        else if (commandWord == "drop") {
            drop(command);
        }
        else if (commandWord == "look") {
            look();
        }
        else if (commandWord == "quit") {
            wantToQuit = quit(command);
        }
        // else command not recognised.
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the
     * command words.
     */
    void Game::printHelp()
    {
        cout << "You are lost. You are alone. You wander" << endl;
        cout << "around at the university." << endl;
        cout << endl;
        cout << "Your command words are:" << endl;
        parser.showCommands();
    }

    /**
     * Pick up an item and place it into the players inventory
     */
     void Game::take(Command command)
     {
       if(!command.hasSecondWord()) {
           // if there is no second word, we don't know what to take...
           cout << "Take what?" << endl;
           return;
       }

       string itemName = command.getSecondWord();

       // Try to pick up the item.
   //    Item *item = player.getCurrentRoom()->containsItem(itemName);

       if (!player.getCurrentRoom()->containsItem(itemName)) {
           cout << "There is no " + itemName + " in this room!" << endl;
       } else {
           player.takeItem(itemName);
           cout << "You pick up the " + itemName << endl;
       }
     }
     /**
      * Drops an item and leaves it in the room
      *
      * @return
      */
     void Game::drop(Command command)
     {
       if(!command.hasSecondWord()) {
           // if there is no second word, we don't know what to drop...
           cout << "Drop what?" << endl;
           return;
       }

       string itemName = command.getSecondWord();

       // Try to drop the item.

       if (!player.containsItem(itemName)) {
           cout << "There is no " + itemName + " in your inventory!" << endl;
       } else {
           player.dropItem(itemName);
           cout << "You drop the " + itemName << endl;
       }
     }

    /**
     * Displays all the items in the room
     */
     void Game::look()
     {
       cout << player.getCurrentRoom()->getItemsString() << endl;
     }

    /**
     * Try to in to one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    void Game::goRoom(Command command)
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            cout << "Go where?" << endl;
            return;
        }

        string direction = command.getSecondWord();


        // Try to leave current room.
        Room *nextRoom = player.getCurrentRoom()->getExit(direction);

        if (nextRoom == NULL) {
            cout << "There is no door!" << endl;
        } else {
            player.newRoom(nextRoom);
            cout << nextRoom->getLongDescription() << endl << endl;
        }
    }

    /**
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    bool Game::quit(Command command)
    {
        if(command.hasSecondWord()) {
            cout << "Quit what?" << endl;
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }