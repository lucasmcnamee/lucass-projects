#include <iostream>
#include <string>
#include "Command.h"
#include "Parser.h"
#include "CommandWords.h"

/**
 *  This class is the main class of the "World of Zuul" application.
 *  "World of Zuul" is a very simple, text based adventure game.  Users
 *  can walk around some scenery. That's all. It should really be extended
 *  to make it more interesting!
 *
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 *
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

using namespace std;

    Parser::Parser()
    {
    //  CommandWords *commands = new CommandWords();
    }

    /**
     * @return The next command from the user.
     */
    Command Parser::getCommand()
    {

      string inputLine; // will hold the full input line
      string word1 = "";
      string word2 = "";
      string restOfWord = "";

      cout << "> ";     // print prompt
      getline (cin, inputLine);

      // Find up to two words on the line.
      int locOfFirstWord;

        word1 = trimString(inputLine, locOfFirstWord); // Word1 will become a commandword if it is valid

        inputLine = inputLine.substr(word1.length(), inputLine.length());

        word2 = trimString(inputLine, locOfFirstWord);

        if(commands.isCommand(word1)){
          return *(new Command(word1, word2));
        } else {
          return *(new Command("", word2));
        }
      }

    void Parser::showCommands()
    {
      commands.showAll();
    }

    /**
     * This will remove any unnecessary spaces from the beginning of the string
     *
     * @PARAM The string and the location of the first word
     * @returs the modified string
     **/
    string Parser::trimString(string &inputLine, int &locOfFirstWord)
    {
      string returnMe; //to be returned at the end of method

      bool tooManySpaces = true;
      while (tooManySpaces == true) {
        locOfFirstWord = inputLine.find_first_of(" ");

        if(locOfFirstWord == 0){
          inputLine = inputLine.substr(1, inputLine.length());
        } else { tooManySpaces = false; }
      }
      returnMe = inputLine.substr(0,locOfFirstWord);

      return returnMe;
    }
