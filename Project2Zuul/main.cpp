/**
* Runs the zuul game and the zuul tets
*
* @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
* @version 2016.10.03
**/
#include <iostream>
#include <string>
#include <map>
#include "CommandWords.h"
#include "Command.h"
#include "Parser.h"
#include "Room.h"
#include "Game.h"
//#include "Parser.h"
//#include "Game.h"
using namespace std;

int main()
{
			Game theGame;
			theGame.play();
}
// To compile use the next line
// g++ main.cpp command.cpp CommandWords.cpp parser.cpp room.cpp game.cpp -o main
