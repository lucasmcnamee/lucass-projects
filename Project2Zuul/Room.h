#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <map>
#include <string>
#include "Item.h"
#include "Inventory.h"

using namespace std;
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * A "Room" represents one location in the scenery of the game.  It is
 * connected to other rooms via exits.  For each existing exit, the room
 * stores a reference to the neighboring room.
 *
 * @author  Michael Kölling and David J. Barnes (Edited by Luke Moran McNamee)
 * @version 2016.10.06
 */

class Room
{
    private:
      string description;
      map<string, Room*> exits;        // stores exits of this room.
      vector<string> keys;
      Inventory roomInventory;

      /**
       * Return a string describing the room's exits, for example
       * "Exits: north west".
       * @return Details of the room's exits.
       */
      string getExitString();

    public:
      /**
       * Create a room described "description". Initially, it has
       * no exits. "description" is something like "a kitchen" or
       * "an open court yard".
       * @param description The room's description.
       */
      Room(string description);

      /**
       * Checks to see if an item is in the inventory
       *
       * @param The item
       * @returns   True: if the item is in the inventory
       *            False: if the item is not in the inventory
       */
      bool containsItem(string itemName);

      /**
       * Return the room that is reached if we go from this room in direction
       * "direction". If there is no room in that direction, return null.
       * @param direction The exit's direction.
       * @return The room in the given direction.
       */
      Room* getExit(string direction);

      /**
       * Define an exit from this room.
       * @param direction The direction of the exit.
       * @param neighbor  The room to which the exit leads.
       */
      void setExit(string direction, Room* neighbor);

      /**
       * @return The short description of the room
       * (the one that was defined in the constructor).
       */
      string getShortDescription();

      /**
       * Return a description of the room in the form:
       *     You are in the kitchen.
       *     Exits: north west
       * @return A long description of this room
       */
      string getLongDescription();


      /**
       * add an item into a room
       *
       * @param the sting of the item's name and the Item itself
       */
      void addItem(string itemName, Item* item);

      /**
       * remove the item from the room
       *
       * @param itemName  Name of the item to remove
       * @return  The removed item
       */
      Item* removeItem(string itemName);

      /**
       * get item in the current room
       *
       * @param   itemName Name of the item
       * @return  The item needed
       */
      Item* getItem(string itemName);

      /**
       * Return a string describing items available in the room.
       * For example, "Items: a bottle of wine"
       *
       * @return Details of the items available in the room
       */
      string getItemsString();
};
#endif
