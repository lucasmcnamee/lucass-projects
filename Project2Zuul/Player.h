#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <map>
#include "Item.h"
#include "Room.h"
#include "Inventory.h"

/**
 * Player class implements the information of the player
 * including the player's current location and the items player currently carries.
 *
 * It also implements take and drop items methods for player to take available
 * items in the room and to leave them in a certain room.
 *
 * canTakeItem method is a special method to determines whether the player should
 * take the item or not depending on the totalweight of the backpack and the item
 *
 *
 * @author Luke Moran McNamee
 * @version 2016.10.06
 */

using namespace std;

class Player
{
    private:
      Room* currentRoom;
      Inventory pack;
    public:

      /**
       * changes the players location
       *
       * @param  The room in which the player enters
       */
      void newRoom(Room* room);

      /**
       * Checks to see if an item is in the inventory
       *
       * @param The item
       * @returns   True: if the item is in the inventory
       *            False: if the item is not in the inventory
       */
      bool containsItem(string itemName);

      /**
       * @return The current room
       */
      Room* getCurrentRoom();

      /**
       *  take the item in the current room
       *
       * @param   itemName The name of the item
       * @return   The item that is picked up
       */
      Item* takeItem(string itemName);

      /**
       * leave the specified item in the current room
       *
       * @param   itemName The name of the item
       * @return  The item is dropped
       */
      Item* dropItem(string itemName);
};
#endif
