/**
Sample program using the Google API and distancematrix
Also provides an example of Java's JSON object

@author Gary Lewandowski (Modified by Lucas Moran McNamee)
Fall 2016
@version 10Aug2016 (Modified on 07Sep2016)
**/


import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonArray;
import java.net.MalformedURLException;
import java.security.cert.Certificate;
import java.net.URL;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.util.ArrayList;
import java.util.HashMap;


public class GoogleTest
{
  static ArrayList<Double> listOfDist = new ArrayList<>();
  static HashMap<Double, String> distToCities = new HashMap<>();
  static String cityVar;
  public static void main(String [] args)
  {
    ReadFile readFile = new ReadFile();
    readFile.readAFile(args);
    for(int i = 0; i < ReadFile.listOfLocs.size(); i++) {
      cityVar = ReadFile.listOfLocs.get(i);
      contactGoogle(ReadFile.listOfLocs.get(i));
    }
    int x = listOfDist.size()-1;
    quickSort(listOfDist, 0, x);

    for (int i = 0; i < listOfDist.size(); i++) {
    System.out.println(distToCities.get(listOfDist.get(i)) + " is " + listOfDist.get(i) + " mi away from Cincinnati.");
    }
  }

  /**
   * This method does exactly what it promises. First it sorts the given data using QuickSort.
   * Then it prints the ordered list
   * @param the list which needs to be sorted.
   **/
   public static void quickSort(ArrayList<Double>sortMePlease, int startIndex, int endIndex)
   {
     if(sortMePlease == null || sortMePlease.size() == 0)
      return;

     if(startIndex >= endIndex)
      return;

     double pivot = sortMePlease.get(startIndex + (endIndex - startIndex) /2 );
     int SI = startIndex;
     int EI = endIndex;
     while(SI <= EI) {
       while(sortMePlease.get(SI) < pivot) {
         SI++;
       }
       while(sortMePlease.get(EI) > pivot) {
         EI--;
       }
       if(SI <= EI) {
         double temp = sortMePlease.get(SI);
         sortMePlease.set(SI, sortMePlease.get(EI));
         sortMePlease.set(EI, temp);
         SI++;
         EI--;
       }
     }
     if (startIndex < EI)
      quickSort(sortMePlease, startIndex, EI);
     if (endIndex > SI)
       quickSort(sortMePlease, SI, endIndex);
   }
  /**
   * This method takes a city and uses it as a destination to find the distacnce from that
   * city to Cincinnati.
   * @param String: the destination city
   **/
  public static void contactGoogle(String destination)
  {
    String destinationWithoutSpaces = destination.replaceAll(" ", "+");
    if(destination != null) {
      String url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Cincinnati&destinations=" + destinationWithoutSpaces + "4&key=AIzaSyDzsmxyZKNfcaVTfuWCPqFhlU76G6YnDOw";
      new GoogleTest(url); // constructs the GoogleTest object. The constructor does the rest
    } else { System.out.println("String " + destinationWithoutSpaces + "is incompatible"); }
  }
/**
* GoogleTest constructor
* @param String urlString is the googleapi string
* @returns nothing, but outputs to the console the distance between the locations
**/
  public GoogleTest(String urlString)
  {
    URL url;
    try {
      // construct a URL
      url = new URL(urlString);
      // Open the connection across the network; receive the response
      HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
      //The next few lines are commented out in case they are needed for later use
      //System.out.println("Response code: " + con.getResponseCode());
      //System.out.println(con.getResponseMessage());
      // the InputStream is the data passed back from the url
      // the next three lines set up a way to read the data
      InputStream ins = con.getInputStream();
      InputStreamReader isr = new InputStreamReader(ins);
      BufferedReader in = new BufferedReader(isr);

      String inputLine;
      String jsonString="";

      while ((inputLine = in.readLine()) != null) // read each line
      { jsonString += inputLine; }

      in.close();
      useJSONToFindDistance(jsonString);
    }
    catch (MalformedURLException e) { e.printStackTrace();}
    catch (IOException e) {e.printStackTrace();}
  }

  public void useJSONToFindDistance(String jsonString)
  {
    /**
    A JSON object consists of named pairs. The JSON we get back from the distance
    matrix api is nested a bit so this pulls out the information in a step-by-
    step fashion
    **/

    // first create the JsonObject so we can easily access the data
      JsonObject jsonObj = Json.createReader(new StringReader(jsonString)).readObject();
      //System.out.println(jsonObj.toString());

      // The distancematrix is stored in an array attached to the name rows
      JsonArray distancematrix = jsonObj.getJsonArray("rows"); // lame name!

      // The distance information is stored as the first item in that array
      JsonObject distanceInfo = distancematrix.getJsonObject(0);
      // distanceInfo is an object holding an array of elements objects
      // elements is an object inside of distanceInfo holding an array of distance objects
      JsonArray elements = distanceInfo.getJsonArray("elements");

      // The information we want is in the first item of the array
      JsonObject distanceAndDuration = elements.getJsonObject(0);
      // and specifically we want the distance from that item
      JsonObject distance = distanceAndDuration.getJsonObject("distance");
      // the distance is stored in readable form under the name "text"

      //Converts the distance from a String to an int and parses accordingly
    //  System.out.println(distance.getString("text"));
      double dist = formatMe(distance.getString("text"));
      listOfDist.add(dist);
      distToCities.put(dist, cityVar);
      }

      public double formatMe(String phraseToParse)
      {
        String phraseWithoutCommas = phraseToParse.replaceAll(",", "");
        double RETURNME = 0.0;

        String delims = "[ ]+";

        String[] splicedStrings = phraseWithoutCommas.split(delims);

        RETURNME = Double.parseDouble(splicedStrings[0]);

        if(splicedStrings[1].equals("ft")) {
          RETURNME = RETURNME / 5280;
        }
        return RETURNME;
      }
}
