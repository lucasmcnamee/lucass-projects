Project 1: Google Maps Distances 

Author: Lucas McNamee

Description: This project uses its ReadFile class to scan a file that has ONE city per line and 
	lists the distances of those cities to Cincinnati in order from nearest to furthest.
	
How to use: navigate to the project's folder in the commandline. 
	Type "java -cp ".;javax.json-1.0.4.jar" GoogleTest <filename>" into the commandline where 
filename is any file with one city per line. I have provided to files to use: Locations.txt or Locations2.txt

Have Fun!