/**
* This class will use the Scanner class to read a file word by word
* @author Lucas Moran McNamee
* @version 07Sep2016
**/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile
{
  static ArrayList<String> listOfLocs = new ArrayList<String>();
  /**
   * echo the given filename
   * @param the scanner for the file
   */
  public void readIt(Scanner infile)
  {
    while (infile.hasNext())
    {
      String word = infile.nextLine();
    //  System.out.println("WORD: " + word);
      listOfLocs.add(word);
    }
  }

  public static void readAFile(String[] fileName)
  {
    if (fileName.length < 1) {
      System.out.println("Are you trying to use a Dvorak keyboard? Please use REAL words. Filename does not exist or has been misplaced.");
    }
    else {
      File fileToRead = new File(fileName[0]);
      try {
        Scanner in = new Scanner(fileToRead);
        ReadFile mainObject = new ReadFile();
        mainObject.readIt(in);
      } catch(IOException e) {
        System.out.println("File Error: "+ e);
      }
    }
  }
}
