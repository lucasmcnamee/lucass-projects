#ifndef STOREHASH_H
#define STOREHASH_H

#include <map>
#include <string>
#include <iostream>
#include "hashtab.h"
/**
* class StoreHash - this class used to simplify the process of comparing each
*  files hashes to itself.
*/
using namespace std;

class StoreHash
{
public:
  StoreHash(string name, map<hashval_t, int> hashes);

void setIt();

/**
* operator++ (Prefix) - overload the ++ operator to increment the internal
*  iterator
*/
map<hashval_t, int>::iterator& operator++();

/**
* operator++ (Prefix) - overload the ++ operator to increment the internal
*  iterator
*/
map<hashval_t, int>::iterator operator++(int);

/**
* operator== - overload the operator to allow comparing the StoreHash name
*  to an object of the same type this will be used to prevent comparing an
*  objects hashes to itself
*/
bool operator==(StoreHash& equality) const;


/**
* getName() - returns the name of the StoreHash
*/
string getName();

/**
* getMap() - returns a map of hashes and line numbers
*/
map<hashval_t, int> getMap();

/**
* first - return the current hash in the iterator, it->first
*/
hashval_t first();

/**
* second - return the current line in the iterator, it->second
*/
int second();

/**
* count - returns hashAndLines.count(countMe)
*/
int count(hashval_t countMe);

/**
* isEnd() - return if interIt is equal to hashAndLines.end()
*/
bool isEnd();

bool isBegin();


/**
* end() - return hashAndLines.end()
*/
map<hashval_t, int>::iterator end();

map<hashval_t, int>::iterator iter();

/**
* find() - finds a key in the map field of this instance
*
* @param key - string, the key to find
* @returns iter - iterator, to the location of the key within the map. If not
*                 within the map, returns iter.end()
*/
map<hashval_t, int>::iterator find(hashval_t key);
private:
  string name;

  map<hashval_t, int> hashAndLines;

  map<hashval_t, int>::iterator interIt;
};
#endif
