#include <iostream>

#include "dirent.h"
#include "HashCompare.h"
#include "SaltGen.h"
#include "stdio.h"
#include "HashMatch.h"
#include "master.h"

/*
* class main - responsible for handling how the program is implemented, for the
*  purpose of good coupling the main class can be changed without impacting how
*  the program will run.
*
* @author Luke Moran McNamee
* @version 2016.12.1
*    Complete (Target grade level N/A)
*/
using namespace std;

string folder = "HashedFiles/";

SaltGen *salty = new SaltGen();
int salt = salty->getSalty();

/**
* runByArg - runs the program using main() parameter arguments
*
*@param argc - int, the number of arguments being passed in
*       argv - char**, the arguments to be passed in
*/
void runByArg(int argc, char** argv);

/**
* runByCin - runs the program using cin argument
*/
void runByCin();

bool printUserInfo(string& fileName1, string& fileName2);

static bool isDirectory(string path);

void directoryHasher(string path, int salt);

int main(int argc, char** argv)
{
  // Allow the user to select which file to read
  if(argc < 2)  // if no param arguments run by using cin
    runByCin();
  else
    runByArg(argc, argv);

  return 1;
}

/**
* runByArg - runs the program using main() parameter arguments
*
*@param argc - int, the number of arguments being passed in
*       argv - char**, the arguments to be passed in
*/
void runByArg(int argc, char** argv)
{
  for(int i = 1; i < argc; i++) {
    string output = argv[i];

    int txt = output.find(".txt");
    if(txt != 0 && !isDirectory(output)) {
      output = output.substr(0, txt);
      Master *hasher = new Master(output + ".txt", folder, salt);
    }
    else if (isDirectory(output)){
      directoryHasher(output, salt);
      std::cout << output << '\n';
      HashCompare *comparer =
      new HashCompare(output + folder, output + folder + "Collision_Report.txt");
    }
  }
}

/**
* runByCin - runs the program using cin argument
*/
void runByCin()
{
  string file1;
  string file2;
    if(printUserInfo(file1, file2)) {
      cout << "hashing..."; // inform user of progress

      Master *hasher1 = new Master(file1, folder, salt);
      Master *hasher2 = new Master(file2, folder, salt);
      cout << "done" << endl << "comparing...";

      string param1 = file1;
      string param2 = file2;
      HashCompare *comparer = new HashCompare(param1, param2,
        folder + "Collision_Report.txt");

      string path = file1.substr(0, file1.find_last_of("/") + 1);
      cout << "done" << endl << "your receipt is located at: \"" + path + folder
       + "Collision_Report.txt\"" << endl;
    }
}

/**
* printUserInfo - prints info so the user knows how to run the file
*/
bool printUserInfo(string& fileName1, string& fileName2)
{
  bool runByFiles = true;
  string fileName;
  pair<string, string>* question = new pair<string, string>
  ("please name the directory or the first file to read...",
  "please name the second file to read...");
  for(int i = 0; i < 2 && runByFiles; i++) {
    string fileName;
    string helpfulMsg;
    if(i == 0) helpfulMsg = question->first;
    else helpfulMsg = question->second;
    cout << endl;
    cout << helpfulMsg << endl;
    cin >> fileName; // get the first file or directory
    cout << endl;
    if(isDirectory(fileName)) {
      directoryHasher(fileName, salt);
      runByFiles = false;
    } else {
      ifstream infile;
      infile.open(fileName.c_str());
      while(!infile) { // while the file cannot be opened
        infile.close();
        cout << endl << "could not open: \"" + fileName + "\"" << endl <<
        "please name a valid file...";
        cin >> fileName;
        infile.open(fileName.c_str());
      }
    }
    if(i == 0) fileName1 = fileName;
    else fileName2 = fileName;
  }
  return runByFiles;
}

/**
* directory - this is used if the input provided is a directory, it will hash
*  all files individually
*
* @param string - the path to the directory
*/
void directoryHasher(string path, int salt)
{
  DIR *dir;
  struct dirent *ent;

  if ((dir = opendir (path.c_str())) != NULL)
  {
    mkdir((path + folder).c_str());
    while ((ent = readdir (dir)) != NULL)
    {
      string input = path + string (ent->d_name);
      if(!isDirectory(input) && input.find(folder) == - 1)
      Master *hash1 = new Master(input, folder, salt);
      else cout << "cannot open" << endl;
    }
    closedir (dir);
  }
  else
  {
    /* could not open directory */
    perror ("");
  }
}

/**
* isDirectory - tests if the path provided is a directory
*
* @param string - the path to the directory
* @returns bool - if the input is a directory
*/
static bool isDirectory(string path)
{
  bool returnMe;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (path.c_str())) != NULL)
  {
    returnMe = true;
  }
  else
  {
    returnMe = false;
    /* could not open directory */
    perror ("");
  }
  return returnMe;
}
