#include <map>
#include <string>

#include "StoreHash.h"

/**
* class StoreHash - this class used to simplify the process of comparing each
*  files hashes to itself.
*/
using namespace std;

StoreHash::StoreHash(string name, map<hashval_t, int> hashes)
{
  this->name = name;
  this->hashAndLines = hashes;
}

void StoreHash::setIt()
{
  interIt = hashAndLines.begin();
}

/**
* operator++ (Prefix) - overload the ++ operator to increment the internal
*  iterator
*/
map<hashval_t, int>::iterator& StoreHash::operator++()
{
  interIt++;
  return this->interIt;
}

/**
* operator++ (Prefix) - overload the ++ operator to increment the internal
*  iterator
*/
map<hashval_t, int>::iterator StoreHash::operator++(int)
{
  map<hashval_t, int>::iterator temp = (*this).interIt; // copy
  ++this->interIt; // pre-increment
  return temp;   // return old value
}

/**
* find() - finds a key in the map field of this instance
*
* @param key - string, the key to find
* @returns iter - iterator, to the location of the key within the map. If not
*                 within the map, returns iter.end()
*/
map<hashval_t, int>::iterator StoreHash::find(hashval_t key)
{
  return hashAndLines.find(key);
}

/**
* operator== - overload the operator to allow comparing the StoreHash name
*  to an object of the same type this will be used to prevent comparing an
*  objects hashes to itself
*/
bool StoreHash::operator==(StoreHash& equality) const
{
  return (this->name==equality.getName());
}

/**
* getName() - returns the name of the StoreHash
*/
string StoreHash::getName()
{
  return this->name;
}

/**
* getMap() - returns a map of hashes and line numbers
*/
map<hashval_t, int> StoreHash::getMap()
{
  return this->hashAndLines;
}

/**
* first() - return the current hash in the iterator, it->first
*/
hashval_t StoreHash::first()
{
  return interIt->first;
}

/**
* second() - return the current line in the iterator, it->second
*/
int StoreHash::second()
{
  return interIt->second;
}

/**
* count - returns hashAndLines.count(countMe)
*/
int StoreHash::count(hashval_t countMe)
{
  return hashAndLines.count(countMe);
}

/**
* isEnd() - return if interIt is equal to hashAndLines.end()
*/
bool StoreHash::isEnd()
{
  cout <<(interIt == hashAndLines.end()) << "isEnd()" << endl;
  std::cout << hashAndLines.size() << '\n';
  return (interIt == hashAndLines.end());
}

bool StoreHash::isBegin()
{
  return (interIt == hashAndLines.begin());
}

/**
* end() - return hashAndLines.end()
*/
map<hashval_t, int>::iterator StoreHash::iter()
{
  return interIt;
}

/**
* end() - return hashAndLines.end()
*/
map<hashval_t, int>::iterator StoreHash::end()
{
  return hashAndLines.end();
}
