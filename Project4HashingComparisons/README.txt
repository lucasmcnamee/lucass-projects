================================================================================
Project 4: Hashing File Comparisons

@Author Luke Moran McNamee
@Version 2016.11.24 "C Level(Incomplete)"

This program is used to compare documents efficiently by implementing hashing,
this will help to decrease the size of each file while also keeping the
contents of the file unread/private.
================================================================================
Progress:
  D level:
  - 2016.11.20 - ok so I reads in a file and uses a given chunk size to input
  that many characters from the file. It also hashes them but I have not gotten
  it to output the hash in any way. maybe a vector?

	- 2016.11.23 - Aw RATS! I came up with an awesome Idea the last time that I
  made an entry about making this into a journal but then I didn't save it! So
  here trying to recreate it after the fact So I made a lot of progress but to
  summarize, I have my program hashing things correctly but I still have not
  implemented a way for the hashes to be stored. As of now they are just
  printed. I decided to make a SaltGen class which will stay static, although
  I don't know how to make a static class yet, it will provide the same salt to
  each instantiation of the HashMaster class. The other thing I mentioned is
  that I was having trouble using the rand() function to generate random
  values. It always returns the same pattern of numbers. I will work on this
  later.

	- 2016.11.24 - I have made a lot of progress. I have now been able to connect
  to the internet to look at the requirements for what is past D level (prior
  to this I was just winging it based on what you had told us about the
  program) So I have completed the requirements for D level!

  C level:
  - 2016.11.24 - So I have come up with a few awesome ideas. This is more of A
  level stuff but I am including at least the idea now. I will include a
  function called processChunk() which will take a string of characters and
  will remove any excess characters like spaces or end-of-line characters while
  also counting the number of end-of-line characters are in the file so an
  index of the chunks within the original file can be stored. The part which I
  think will be really cool will be a file that isn't actually any c++ code.
  this will be a file that is just a text document with the characters or words
  which the user wants to exclude from the file. This is useful for when this
  program is used when foul play is expected (to circumvent and
  anti-plagiarism engines). I will implement this later although I have written
  the file.

  - 2016.11.26 - Ok So I have just gotten around to correctly outputting my
  hashes into a file. I did this the simplest way I could think of which was to
  concatenate all of the hashes into a string then use the << operator with the
  ofstream class. I realize that this is probably not the most efficient way to
  do this because each time the string will have to add a more to itself until
  I output it. So eventually I would like to get a proper handle of the write()
  function because I have used it but it is complicated to work with. Not sure
  what to do now because I am on a bus again so I will just clean up my code and
  work on output the hash values of the lines of my code.
    right now I get an output like this:
    10076220634646064234
    10760560840577686376
    11191400573439034883
    12420790086547115515
    13066542034353365938
    13794063277745698773
    15079241095587288660
    15727333838442028976
    16038167823782580637
    18244453595429245230
    2413986261931135695
    3081120256198174851
    3857065761681630731
    3916470201223177163
    4111872734835082906
    4356424363695134420
    4510594665289552214
    5955119835963314023
    6759460940543614943
    6907603896609685447
    7031105479438730239
    7060313359869559662
    7145174237235366140
    8223518220351547047
    9521860592092952950
  I am wondering about this because if you notice the first digit is counting
  upwards from 1-9... I will test this but it could be just a fluke.

  - 11.28.2016 - apparently the map iterator will output an ordered list! Very
  interesting, You mentioned that this would happen with a set I believe and it
  is interesting. I have my code now output the line numbers and the filename.

  - 12.1.2016 - I have the last few days helping other people with their
  projects while only making minor progress on my project but today I will go
  ahead and write my HashCompare class which will compare (as of now 2) hashed
  files but later will hash a whole directory. I am going to create a vector to
  store the names of each file so the tool can be expanded to work on multiple
  files or a directory. I am wondering why we are using the window. As I see it
  it makes it harder to find a comparison (say only lines 1 and 2 are the same
  but since line 3 is hashed on to it the final hash values will be different)
  and also it takes more time since we have to re hash the same string many
  3 times. If we get rid of this shouldn't it reduce our running time by a
  factor of 3?

  - 12.5.2016 - Ok over the past few days I've actually have been working like
  crazy on this progress. HashCompare Now fully works. The way I was able to
  make it work well is by creating the new class HashMatch which stores a map
  of all of the lines where there are collisions between two files and also the
  two file names. This class was inspired by the Command class from zuul which
  was very simple and was mostly implemented to organized the project. Now
  Instead of having a map<pair<string, string>, map<int, int>> I have a short
  class which stores all of this information yet in a much more readable
  fashion. I really should implement a new class to make the comparing of my
  hashes easier but I need to do a few other things first. I need to now get
  back to removing white space. Since we have been given a little more time on the
  project I will use this to do all of the suggestions from a level as well as
  a few improvements of my own. I have implemented print receipt which takes the
  info from the comparison and print it into a file.
    next (and really final) steps:
      first. Allow the input to be flexible. Identify a file or directory and
        execute the input accordingly.
      second. Remove whitespace and allow user to allow user modification of the
        excluded characters
      third. add run time stats

  - 12.9.2016 - ask anyone in the lab, I have done little else but this project
  the past week.
