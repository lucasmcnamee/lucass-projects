#include "SaltGen.h"

#include <iostream>
/**
* class SaltGen - produces a random value and allows other classes to retrieves
*  that value. The value will be randomized only at instantiation so each class
*  will recieve the same value
*
* @author Luke Moran McNamee
* @version 2016.12.1
*/
using namespace std;

SaltGen::SaltGen()
{
  srand(time(NULL));
  SALT_VALUE = rand() % 512 + 1;
}

int SaltGen::getSalty()
{

  return SALT_VALUE;
}
