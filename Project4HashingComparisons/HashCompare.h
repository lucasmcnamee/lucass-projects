#ifndef HASHCOMPARE_H
#define HASHCOMPARE_H

#include <string>
#include <map>
#include <vector>

#include "StoreHash.h"
#include "HashMatch.h"

using namespace std;

/**
* class HashCompare will take two hashed files and compare them for
*  similarities. The class's 'printReceipt' will print out the results into a
*  file called 'Collision_Report.txt'
*
*@author Luke Moran McNamee
*@Version: Final
*/
class HashCompare
{
private:
  // varibles
  string hash1;
  string hash2;
  // stores the names of the files to be compared
  string report;
  // path the output containing the information about collisions
  vector<string> fileStore;
public:
  /**
  * compareFiles - will take two hashed files and evaluate how similar the
  *  two files are.
  *
  * @param hash1 - the name of the first hashed file
  *        hash2 - the name of the second hashed file
  *        report - where to store the the collision report document
  */
  HashCompare(string hash1, string hash2, string report);

  HashCompare(string directoryPath, string report);

  /**
  * buildStoreHash - takes the fileName and separates the file into a vector
  *  where the index is the line number and the value is the hash value
  *
  * @param string - the file name
  * @returns storedAway - StoreHash*, an object containing all of the relevant
  *                       information to compare a file by hashes.
  */
  StoreHash* buildStoreHash(string file);

  /**
  * compareValues - find all of the comparison between two hashed files. each
  *  hashed file is stored within a object of class StoreHash. The function find
  *  if there are comparisons in the file, then will print a "reciept", a file
  *  containing the information of the files and if there are line matches.
  *
  * @param hashStore - StoreHash*, a vector containing member of StoreHash class
  *                    which stores the data of a hash in a format that includes
  *                    an internal iterator.
  */
  void compareValues(vector<StoreHash*> hashStore);

  /**
  * to_int - this will convert a string to an int if the string is of int
  *  characters. I really couldn't find a simple way to do this using std so
  *  built my own function.
  *
  * @param string - the string to be converted
  * @returns int - the int after being converted
  */
  int to_int(string input);

  /**
  * printReceipt - outputs relevant information about the comparison into file.
  *
  * @param vector - contained is all of the information about the comparisons
  *  stored as HashMatch's
  */
  void printReceipt(vector<HashMatch*> collisions);
};
#endif
