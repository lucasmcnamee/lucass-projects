#ifndef SALTGEN_H
#define SALTGEN_H

#include "time.h"

/**
* class SaltGen - produces a random value and allows other classes to retrieves
*  that value. The value will be randomized only at instantiation so each class
*  will recieve the same value
*
* @author Luke Moran McNamee
* @version 2016.12.1
*/
class SaltGen
{
  private:
    int SALT_VALUE;
  public:
    SaltGen();
    int getSalty();
};
#endif
