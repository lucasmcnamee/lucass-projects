#include <string>
#include <fstream>
#include <iostream>
#include "dirent.h"

#include "StoreHash.h"
#include "HashCompare.h"
/**
* class HashCompare will take two hashed files and compare them for
*  similarities. The class's 'printReceipt' will print out the results into a
*  file called 'Collision_Report.txt'
*
*@author Luke Moran McNamee
*@Version: Final
*/
using namespace std;

/**
* compareFiles - will take two hashed files and evaluate how similar the
*  two files are.
*
* @param hash1 - the name of the first hashed file
*        hash2 - the name of the second hashed file
*        report - where to store the the collision report document
*/
HashCompare::HashCompare(string hash1, string hash2, string report)
{
  this->report = report;
  vector<StoreHash*> hashes;

  ifstream file1;
  file1.open(hash1.c_str());
  if(!file1) cout << endl << "!COULD NOT OPEN " << hash1 << "!!" << '\n';
  file1.close();

  StoreHash* store1 = buildStoreHash(hash1);
  store1->setIt(); // set the iterator to the beginning value of the map
  hashes.push_back(store1);

  ifstream file2;
  file2.open(hash2.c_str());
  if(!file2) cout << endl << "!COULD NOT OPEN " << hash2 << "!!" << '\n';
  file2.close();

  StoreHash* store2 = buildStoreHash(hash2);
  store2->setIt(); // set the iterator to the beginning value of the map
  hashes.push_back(store2);

  compareValues(hashes);
}

/**
* Constructor for directory
*/
HashCompare::HashCompare(string directoryPath, string report)
{
  this->hash1 = directoryPath;
  this->report = report;
  vector<StoreHash*> hashes;
  // store all of the files' hashes within the vector of StoreHash
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (directoryPath.c_str())) != NULL) {
    /* run all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      string stringMe = ent->d_name;
      int isHashed = stringMe.find("_HASH");
      if(!(stringMe == "." || stringMe == ".." || isHashed == -1)){
        fileStore.push_back(stringMe); // add to the list files to hash
      }
    }
    closedir (dir);
  } else {
    /* could not open directory */
    perror ("");
  }
  for (int i = 0; i < fileStore.size(); i++) {
    StoreHash* hashStore = buildStoreHash(fileStore[i]);
    // build object to store hashes
    hashStore->setIt(); // set the iterator to the beginning value of the map
    hashes.push_back(hashStore);
  }
  compareValues(hashes);
  // compare the values
}

/**
* buildStoreHash - takes the fileName and separates the file into a vector where
*  the index is the line number and the value is the hash value
*
* @param string - the file name
* @returns storedAway - StoreHash*, an object containing all of the relevant
*                       information to compare a file by hashes.
*/
StoreHash* HashCompare::buildStoreHash(string file)
{
  ifstream infile;
  infile.open(hash1+file.c_str());

  string fileName;
  string sizeStr;
  map<hashval_t, int> returnMe;

  getline(infile, fileName);
  getline(infile, sizeStr);
  // go through first two lines of the file to get the fileName and chunk_size
  sizeStr = sizeStr.substr(sizeStr.find_last_of(" "), sizeStr.size());
  int chunk_size = to_int(sizeStr);

  while(infile.peek() && !infile.eof() && infile.good())
  {
    int index;
    string space = "";
    hashval_t hash;

    infile >> index; // get line number
    infile >> space; // get space
    infile >> hash; // get hash value
    infile >> space; // get endl
    if(!((index <= 0) || (hash == 0))){
      returnMe[hash] = index;
    }
  }
  infile.close();
  StoreHash* storedAway = new StoreHash(file, returnMe);
  return storedAway;
}

/**
* compareValues - find all of the comparison between two hashed files. each
*  hashed file is stored within a object of class StoreHash. The function find
*  if there are comparisons in the file, then will print a "reciept", a file
*  containing the information of the files and if there are line matches.
*
* @runtime n lg(n) - where n is the number of files
* @param hashStore - StoreHash*, a vector containing member of StoreHash class
*                    which stores the data of a hash in a format that includes
*                    an internal iterator.
*/
void HashCompare::compareValues(vector<StoreHash*> hashStore)
{
  // store the results by class
  vector<HashMatch*> resultsGoHere;
  // first loop: to go though the vector from the parameter
  for(int it1 = 0; it1 < hashStore.size(); it1++) {
    StoreHash* store1 = hashStore[it1];
    string f1 = hashStore[it1]->getName();

    // second loop: through the vector from the parameter again to compare
    for(int it2 = 1; it2 < hashStore.size(); it2++) {
      if(it1 > it2)  continue; // prevent comparing 1 to 2 then 2 to 1
      StoreHash* store2 = hashStore[it2];
      string c1 = hashStore[it2]->getName();
      HashMatch *results = new HashMatch(c1, f1);
      // HashMatch to hold the matches for this file
      // third loop: to iterate over each item of the map in store1
      for(; store2->iter() != store2->end(); ++(*store2)) {
        cout << store1->first() << endl;
        int count = store1->count(store2->first());
        // get the number of times there is a match then loop that many times
        for (int i = 0; i < count; i++) {
          map<hashval_t, int>::iterator iter = store1->find(store2->first());
          // iter gets the location of the match within the map
          if(iter != store1->end())
            results->addLineMatch(iter->second, store2->second());
        }
      }
      if(results->size() != 0)
        resultsGoHere.push_back(results);
    }
  }
  printReceipt(resultsGoHere);
}

/**
* to_int - this will convert a string to an int if the string is of int
*  characters. I really couldn't find a simple way to do this using std so
*  built my own function.
*
* @param string - the string to be converted
* @returns int - the int after being converted
*/
int HashCompare::to_int(string input)
{
  int returnMe = 0;
  for(int i = 0; i < input.size(); i++)
  {
    int character = input[i] - 48;

    returnMe = (returnMe * 10) + character;
  }
  return returnMe;
}

/**
* printReceipt - outputs relevant information about the comparison into file.
*
* @param vector - contained is all of the information about the comparisons
*  stored as HashMatch's
*/
void HashCompare::printReceipt(vector<HashMatch*> collisions)
{
  ofstream outfile;
  outfile.open(report.c_str()); // use report for a path to output the file

  outfile << endl;
  for (size_t i = 0; i < collisions.size(); i++) {
    HashMatch* match = collisions[i];

    vector<pair<int, int>*> lines = match->getLineMatches();
    if(lines.size() == 0) continue;
    string file1 = match->getFileNames().first;
    string file2 = match->getFileNames().second;

    outfile << "in \"" + file1 + "\" there were " << lines.size()
    << " collisions with \"" + file2 + "\"\ncollisions occured at: \n" << endl;

    string fileHeader = file1 + ":       " +  file2 + ": \n";
    string formatting = "-> ";
    string s = " "; // add a space for formatting

    while(formatting.size() < fileHeader.size() / 2)
    formatting = s + formatting;

    string fileTable = "";
    for (int i = 0; i < match->size(); i++)
    {
      if((lines[i]->first > 0) && (lines[i]->second > 0))
      {
        outfile << formatting << lines[i]->first << formatting
        << lines[i]->second << '\n';
      }
    }
  }
  outfile.close();
}
