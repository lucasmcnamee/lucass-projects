#ifndef HASHMATCH_H
#define HASHMATCH_H

#include <string>
#include <vector>
/**
* class HashMatch - this class will store the data for when there is a match
*  between two files. All this class does is stores the two file names, and the
*  two line numbers and allows accessor functions to retrieve this information.
*
* @author Luke Moran McNamee
* @version 2016.12.5
*/
using namespace std;

class HashMatch
{
public:
  HashMatch(string firstFile, string secondFile);

  /**
  * compare() - to return true if the filePair is equal to the parameters. Note:
  *  comparison will return true if first equals filePair.second and second
  *  equals filePair.first. The comparisons checks if the values of the pair is
  *  equal, the order does not matter.
  *
  * @param the right Hand Argument of the operator
  * @returns true if the filePair has the same values as parameter.
  */
  bool compare(string first, string second);

  /**
   * accessFileNames - returns a pair of the file names
   */
  pair<string, string> getFileNames();

  /**
  * addLineMatch - adds a line match to the vector storing all of the line
  *  matches
  *
  * @param int - the line number of the match in the first file
  *        int - the line number of the match in the second file
  */
  void addLineMatch(int line1, int line2);

  /**
  * size() - returns the number of valid items within the vector.
  */
  int size();

  /**
   * accessLineMatches - returns a vector of the lines where matches occur in the
   *  files, the key is the line in the first file, the value is the line in
   *  the second file
   */
  vector<pair<int,int>*> getLineMatches();


private:
  pair<string, string> filePair;
  // the name of the first and the second files, respectively
  vector<pair<int,int>*> lineMatches;
  // the lines where there are matches within the file
  int sizeOfVector = 0;
};
#endif
