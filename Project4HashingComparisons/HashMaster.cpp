#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <wctype.h>

#include "HashMaster.h"
#include "hashtab.h"
#include "SaltGen.h"
/*
* Class HashMaster - This file handles the hashing portion of the project. This
*  will hash a file into chunks of the size that is equal to the global varible
*  chunk_size. Then it will output the chunk into the desired output file.
*
* HashTab.h was provided by Eric S. Raymond from his Comparator program which
*  is available under the GNU license. All credit for its implementation go
*  exclusively to him.
*
* @author Luke Moran McNamee (with contributions from Eric S. Raymond)
* @version 2016.21.16
*    "A Level(Incomplete)"
*/
using namespace std;

/**
* master - this function executes the appropriate functions in order to
*  hash the given file.
*
* @param fileName - string the name of the file to be hashed
*        folder - string, the folder to put the files into within fileName's
*                 directory
*        salt - char, a character to randomize the hash values
*/
void HashMaster::master(string fileName, string folder, char salt)
{
  this->inputfile = fileName;
  this->salt = salt;
  if(!canRun(fileName)) return;
  // if the file can be opened then hash
  chunkify(fileName);
  // read in the file and separate it into 'chunks' which will hashed

  int lastDir = fileName.find_last_of("/");
  if(lastDir == -1) lastDir = 0; // fileName is within current directory
  string outputPath = fileName.insert(lastDir+1, folder); // output to folder

  outputPath = outputPath.insert(outputPath.find(".txt"), "_HASH");
  // filePath will now be ./folder/fileName_HASH.txt
  ofstream outfile;
  outfile.open(outputPath.c_str());
  // instantiate the location of the output

  string output = "file: " + inputfile + '\n';
  output = output + "Characters per chunk: " + to_string(chunk_size) + '\n';
  outfile << output + '\n';
  // instantiate the string onto which the hashes will be concatenated
  for (map<hashval_t, int>::iterator it=chunkWithIndex.begin();
  it!=chunkWithIndex.end(); ++it)
  {
    outfile << it->second << " " << it->first << '\n';
  }
  outfile << output;
  // write output, the string holding all of the hashes, to the file
  outfile.close();
}

/**
* canRun - tests if the given string infile a file which can be opened
*
*@param string - the fileName to be opened
*/
bool HashMaster::canRun(string fileName)
{
  ifstream infile;
  infile.open(fileName.c_str());

  if(!infile)
  {
    cout << "CANNOT OPEN THE FILE!! " << fileName << endl << endl;
    infile.close();
    return false;
  } else {
    cout << fileName << " checksout" << endl << endl;
    return true;
  }
  infile.close();
}

/**
* chunkify - uses readChunk() to get a string of characters from the infile.
*  then handles how to hash the string
*
* @param fileName - string, the file name to be passed to readChunk
*/
void HashMaster::chunkify(string fileName)
{
  ifstream infile;
  infile.open(fileName.c_str());

  infile.seekg(0, infile.end);
  int length = infile.tellg();
  infile.seekg(0, infile.beg);
  // get length of file, then set the file iterator back to the beginning
  int lengthOfChunk = chunk_size;
  for(int i = 0; i * chunk_size < length; i++)
  {
    if(length - ((i + 1) * chunk_size) < 0)
      lengthOfChunk = (lengthOfChunk % chunk_size);
    // length mod chunk_size to get the characters at the end of the file that
    //   wont fill a full chunk
    string chunk = "";
    int count = 0;
    while(infile.peek() && !infile.eof() && count < lengthOfChunk)
    {
      char ch = infile.get();
      if((!iswspace(ch)) || (ch == '\n'))
      {
        chunk = chunk + ch;
        count++;
      }
    }
    if(i == 0) { // first iteration gets the first chunk
      lastLastString = chunk;
    } else if(i == 1) { // second iteration gets the second chunk
      lastString = chunk;
    } else if(2 * chunk_size > length) {
      // if the file is less than 3 lines only hash the first then the second
      lastString = "";
      lastLastString = "";
      hashChunk(chunk);
    } else { // now that our chunk is large enough we hash it
      hashChunk(chunk);

      lastLastString = lastString;
      lastString = chunk;
      // advance the window by one chunk
    }
  }
  infile.close();
  /*ifstream infile;
  infile.open(fileName.c_str());

  infile.seekg(0, infile.end);
  int length = infile.tellg();
  infile.seekg(0, infile.beg);
  // get length of file, then set the file iterator back to the beginning
  int lengthOfChunk = chunk_size;
  for(int i = 0; i * chunk_size < length; i++)
  {
    if(length - ((i + 1) * chunk_size) < 0)
      lengthOfChunk = (lengthOfChunk % chunk_size);
    // length mod chunk_size to get the characters at the end of the file that
    //   wont fill a full chunk

    char * buffer = new char [lengthOfChunk];
//ctype.h is whitespace
    infile.read(buffer, lengthOfChunk);
    // read data as a block:
    string chunk = buffer;
    delete[] buffer;
    chunk = chunk.substr(0, lengthOfChunk);

    if(i == 0) { // first iteration gets the first chunk
      lastLastString = chunk;
    } else if(i == 1) { // second iteration gets the second chunk
      lastString = chunk;
    } else if(2 * chunk_size > length) {
      // if the file is less than 3 lines only hash the first then the second
      lastString = "";
      lastLastString = "";
      hashChunk(chunk);
    } else { // now that our chunk is large enough we hash it
      hashChunk(chunk);

      lastLastString = lastString;
      lastString = chunk;
      // advance the window by one chunk
    }
  }
  infile.close();
*/
}

/**
* countReturns - count the number of end-of-line characters in the file and
*  return it as a string so it can be easily concatenated onto a string for the
*  output.
*
*@param chunkString - string, number of returns
*/
int HashMaster::countReturns(string chunkString)
{
  int EOL = 0;
  for(int i = 0; i < chunkString.size(); i++)
  {
    char c = '\n';
    if(chunkString[i] == c){
      EOL++;
       //(T) cout << "found EOL on " << i << " " << chunkString[i] << '\n';
    }
  }
  lineCount = lineCount + EOL;
  //(T) cout << "found lineCount: " << lineCount << '\n';
  int returnMe = (lineCount + 1) - EOL;
  return returnMe;
}

/**
* hashChunk - given a string to be hashed this function hashes that string
*  then stores the hash into the global map "chunkStore"
*  hash process - For a text string X[0]...X[k-1] of bytes define its hash to be
*   --> T[X[0],0] xor T[X[1],1] xor ... xor T[X[k-1],k-1]
*
* @param string - the string to be hashed
*/
void HashMaster::hashChunk(string chunkString)
{
  string chunk = lastLastString + lastString + chunkString;
  // concatenate the chunk with the previous two chunks

  int lineCount;
  if(lastLastString == "") // there are fewer than three lines
    if(lastString == "") // there are fewer than two lines
      lineCount = countReturns(chunkString); // count individually
    else
      lineCount = countReturns(lastString);
  else // proceed normally
    lineCount = countReturns(lastLastString);
  hashval_t final_chunk = (hashval_t) magicbits[chunk[salt]][salt];
  /* using xor final_chunk will change with each iteration, first value is
  salt which is static to all of the files being hashed salt randomizes
  the value */
  for (size_t i = 0; i < chunk.size(); i++) {
    final_chunk = final_chunk ^ magicbits[chunk[i]][i];
    i++;
  }

  long long unsigned int toString = final_chunk;
  //string chunkStringThing = to_string((long long unsigned int) toString);
  // convert to a string for convenience of concatenation and ease of output
  chunkWithIndex[final_chunk] = lineCount;
}
