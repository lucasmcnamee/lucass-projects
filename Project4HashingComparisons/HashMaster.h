#ifndef HASHMASTER_H
#define HASHMASTER_H

#include "hashtab.h"
#include <fstream>
#include <map>
#include <vector>
/*
* Class HashMaster - This file handles the hashing portion of the project
*
* HashTab.h was provided by Eric S. Raymond from his Comparator program which
*  is available under the GNU license. All credit for its implementation go
*  exclusively to him.
*
* @author Luke Moran McNamee (with contributions from Eric S. Raymond)
* @version 2016.11.15
*    "D Level(Incomplete)"
*/
using namespace std;

class HashMaster
{
public:

  /**
  * master - this function executes the appropriate functions in order to
  *  hash the given file.
  *
  * @param fileName - string the name of the file to be hashed
  *        folder - string, the folder to put the files into within fileName's
  *                 directory
  *        salt - char, a character to randomize the hash values
  */
  void master(string fileName, string folder, char salt);

private:
  // varibles

  const signed int chunk_size = 40;

  string lastString = "";
  string lastLastString = "";
  //these will hold the value of the most recent and the second most recent
    //strings of characters to simulate the 'window'

  char salt;

  string inputfile;
  string outputfile;

  int lineCount = 0; // total number of lines in the file

  vector<string> excludedWords;

  map<hashval_t, int> chunkWithIndex;

  // functions

  /**
  * canRun - tests if the given string is a file which can be opened
  *
  *@param string - the fileName to be opened
  */
  bool canRun(string fileName);

  /**
   * getExclusions - retrieves the excluded words and then stores them into a
   *  vector
   */
  void getExclusions();

  /**
  * chunkify - uses readChunk() to get a string of characters from the infile.
  *  then handles how to hash the string
  *
  * @param fileName - string, the file name to be passed to readChunk
  */
  void chunkify(string fileName);

  /**
  * countReturns - goes through a chunk and parses out unneeded character, like
  *  spaces or end-of-line characters which will not change the outcome of the
  *  hash and also may prevent attempts to circumvent hash matching. Will also
  *  count the number of end-of-line characters in order to index the chunk in
  *  the file.
  *
  * @param string - the chunk to be processed
  * @returns string - the smaller string with the excess characters removed
  */
  int countReturns(string chunkString);

  /**
  * hashChunk - given a string to be hashed this function hashes that string
  * then stores the hash into the global map "chunkStore"
  *
  * @param string - the string to by hashed
  */
  void hashChunk(string chunkString);

  /**
  * writeChunks - using the global map chunkWithIndex this function writes
  * each chunk to the output file.
  */
  void writeChunks();
};
#endif
