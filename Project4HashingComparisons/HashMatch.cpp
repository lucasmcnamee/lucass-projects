#include "HashMatch.h"

#include <string>
#include <iostream>

/**
 * class HashMatch - this class will store the data for when there is a match
 *  between two files. All this class does is stores the two file names, and the
 *  two line numbers and allows accessor functions to retrieve this information.
 *
 * @author Luke Moran McNamee
 * @version 2016.12.5
 */
 using namespace std;

 HashMatch::HashMatch(string firstFile, string secondFile)
 {
   filePair = pair<string, string>(firstFile, secondFile);
 }

 /**
 * compare() - to return true if the filePair is equal to the parameters. Note:
 *  comparison will return true if first equals filePair.second and second
 *  equals filePair.first. The comparisons checks if the values of the pair is
 *  equal, the order does not matter.
 *
 * @param the right Hand Argument of the operator
 * @returns true if the filePair has the same values as parameter.
 */
 bool HashMatch::compare(string first, string second)
 {
   bool returnMe = false;
   if((first == filePair.first) && (second == filePair.second)) {
     returnMe = true;
   }
   else if((second == filePair.first) && (first == filePair.second)) {
     returnMe = true;
   }
   return returnMe;
 }

 /**
  * accessFileNames - returns a pair of the file names
  */
 pair<string, string> HashMatch::getFileNames()
 {
   return filePair;
 }

 /**
 * size() - returns the number of valid items within the vector.
 */
 int HashMatch::size()
 {
   return sizeOfVector;
 }

 /**
 * addLineMatch - adds a line match to the vector storing all of the line
 *  matches
 *
 * @param int - the line number of the match in the first file
 *        int - the line number of the match in the second file
 */
 void HashMatch::addLineMatch(int line1, int line2)
 {
   pair<int,int> *pushMe = new pair<int,int>(line1, line2);
   lineMatches.push_back(pushMe);
   sizeOfVector++;
 }

 /**
  * accessLineMatches - returns a map of the lines where matches occur in the
  *  files, the key is the line in the first file, the value is the line in
  *  the second file
  */
 vector<pair<int, int>*> HashMatch::getLineMatches()
 {
   return lineMatches;
 }
