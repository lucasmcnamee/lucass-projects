package edu.xavier.csci260.mcnameel.hw5.service;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/7/2017.
 Create a new service interface and implementation:  PigLatinService and PigLatinServiceImpl
 put the service class in the proper package
 This contains the business log for the PigLatinController services.
 Should contain at least one method for the translate method in the controller.
 The service should translate the words passed in, and track their stats.
 Add timing logging to your service layer so that it is clear in the log file how long a call
 to the main service function takes.

 */
@FunctionalInterface
public interface PigLatinService {

    String translate(String word);
}
