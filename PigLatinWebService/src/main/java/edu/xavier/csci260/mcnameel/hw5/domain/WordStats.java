package edu.xavier.csci260.mcnameel.hw5.domain;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/4/2017.
 */
public class WordStats {

    private String word ; // the word
    private String conversion; // the pig latin form of the word
    private Integer timesSeen; // number of times seen

    public WordStats() {
        // This is for the JdbcTemplate's RowMapper class
    }

    public WordStats(String word, String conversion, Integer timesSeen) {
        this.word = word;
        this.conversion = conversion;
        this.timesSeen = timesSeen;
    }

    @Override
    public String toString() {
        return String.format(
                "WordStats[word='%s', conversion='%s', timesSeen=%d]", word, conversion, timesSeen);
    }


    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }

    public Integer getTimesSeen() {
        return timesSeen;
    }

    public void setTimesSeen(Integer timesSeen) {
        this.timesSeen = timesSeen;
    }
}
