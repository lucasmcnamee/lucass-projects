package edu.xavier.csci260.mcnameel.hw5.controller;

import edu.xavier.csci260.mcnameel.hw4.util.Util;
import edu.xavier.csci260.mcnameel.hw5.dal.WordTrackerDAO;
import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import edu.xavier.csci260.mcnameel.hw5.service.PigLatinService;
import edu.xavier.csci260.mcnameel.hw5.service.PigLatinServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


/**
 * This part of the homework builds on the previous 2 sections and previous homework.
 * In this part you will be using the timing function you created in part 2 in a new web service.
 * You will also be given less detail/direction on the service layer and will be expected to complete this part
 * by applying ideas you have been working on in the previous parts.

 Create a new Controller in the HW5-2 repo: PigLatinController
 /piglatin/translate HTTP POST
 {words: [] }
 words is a list of Strings assume single words (i.e.  “this, that” and not “This and That, and this”
 Return a json response  with the following format:
 {words: [ ]
 words is a list of of translated strings.
 takes a body with json format of
 Put the controller in the proper package
 mappings:
 */
@Controller
class PigLatinController implements Util {

    private final PigLatinService service;
    private final WordTrackerDAO wordTrackerDAO;

    @Autowired
    public PigLatinController(WordTrackerDAO wordTrackerDAO) {
        service = new PigLatinServiceImpl();
        this.wordTrackerDAO = wordTrackerDAO;
    }

    /**
     * add a word to the database
     * /piglatin/translate HTTP POST
     * @param word to translate and, if not already tracked, to track
     */
    @RequestMapping(value = "/piglatin/translate/{word}")
    @ResponseBody
    public String post(@PathVariable("word")String word) {

        String[] returnMe = {""};

        timing( e -> {

            Optional<List<WordStats>> words = wordTrackerDAO.find(word);

            if(words.isPresent()) { // if the translation exists get it

                returnMe[0] = words.get().get(0).getConversion();

            } else { // else create it with the translation to track the stats

                returnMe[0] = service.translate(word);

                wordTrackerDAO.createWords(word, returnMe[0]);

            }
        });

        return returnMe[0];
    }
}
