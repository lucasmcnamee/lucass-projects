package edu.xavier.csci260.mcnameel.hw5.dal;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * class ___ is a member of the hw5-2-mcnameel project.
 * <p>
 * Created by Luke on 3/4/2017.
 */
public interface WordTrackerDAO {
    void wordUpdate(String word, String conversion) throws WordConversionException;

    Optional<List<WordStats>> find(String word);

    void createWords(String word, String conversion);

    Optional<List<WordStats>> findAll();

    class WordConversionException extends Exception implements Serializable {

        final String word;

        public WordConversionException(String word) {

            this.word = word;

            final Logger logger = LoggerFactory.getLogger(WordTrackerDAO.class);

            logger.info("Error: Query for word " + word + " could not be completed.");
        }

        public String getWord() {
            return word;
        }
    }
}
