package edu.xavier.csci260.mcnameel.hw5.controller;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import edu.xavier.csci260.mcnameel.hw5.service.WordStatsService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/4/2017.
 */
@Controller
class WordStatsController {

    private final WordStatsService service;

    public WordStatsController(WordStatsService wordStatsService) {
        this.service = wordStatsService;
    }

    /**
     * return stats for a single word provided in the url (this could be a list of words)
     * @param word - the word to query
     * @return List of WordStats
     */
    @RequestMapping(value = "/word/stats/{word}", method = RequestMethod.GET)
    @ResponseBody
    public List<WordStats> get(@PathVariable("word")String word) {

        return service.findWord(Optional.of(word)).orElse(null);
    }

    /**
     * @return - all words’ stats from the database as a json list of wordstats
     */
    @RequestMapping(value = "/word/all"/*, method = RequestMethod.GET*/)
    @ResponseBody
    public List<WordStats> getAll() {

        return service.findWord( Optional.empty() ).orElseGet( null );
    }

    /**
     * add a word to the database
     * @param wordStats the word to add
     */
    @RequestMapping(value = "/word/update"/*, method = RequestMethod.POST*/)
    @ResponseBody
    public String post(@RequestBody WordStats wordStats) {

        return service.update( wordStats ).orElseGet( null ).toString();
    }
}
