package edu.xavier.csci260.mcnameel.hw5.service;

import edu.xavier.csci260.mcnameel.PLatin.PLatinImpl;
import edu.xavier.csci260.mcnameel.hw5.dal.WordTrackerDAO;
import edu.xavier.csci260.mcnameel.hw5.dal.WordTrackerDAOImpl;
import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/6/2017.
 */
@Service
@Component
public class WordStatsServiceImpl implements WordStatsService {

    private final WordTrackerDAO dal;

    final PLatinImpl converter = new PLatinImpl();

    @Autowired
    public WordStatsServiceImpl(WordTrackerDAOImpl wordTrackerDAO) {

        this.dal = wordTrackerDAO;
    }

    /**
     * check to see if word exists in database, if not create word with count of 1 and save in database.
     *  If word does exist, call  the wordUpdate method
     * @param wordStats the word to update
     */
    @Override
    public Optional<List<WordStats>> update(WordStats wordStats) {

        String word = wordStats.getWord();

        Optional<List<WordStats>> stats = dal.find(word);

        if (stats.isPresent()) { // wordUpdate

            List<WordStats> words = stats.get();

            for(int i = words.size() - 1; i >= 0; --i) {

                WordStats curWord = words.get(i);

                try {

                    dal.wordUpdate(curWord.getWord(), curWord.getConversion());

                } catch (WordTrackerDAO.WordConversionException ex) {

                    final Logger logger = LoggerFactory.getLogger(WordStatsServiceImpl.class);

                    logger.info("Error: Structure of the program has been changed to allow for a word to not match " +
                            "its conversion \n", ex);
                }
            }
        } else { // create it

            dal.createWords(word, wordStats.getConversion());

        }

        return dal.find(word);
    }

    /**
     * This takes an optional parameter, if optional is populated call find other wise call findAll
     * @param checkMe the optional to check
     */
    @Override
    public Optional<List<WordStats>> findWord(Optional<String> checkMe) {

        return checkMe.map(dal::find).orElseGet(dal::findAll);
    }
}
