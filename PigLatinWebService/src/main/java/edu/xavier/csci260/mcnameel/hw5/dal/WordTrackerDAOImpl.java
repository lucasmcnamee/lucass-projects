package edu.xavier.csci260.mcnameel.hw5.dal;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import edu.xavier.csci260.mcnameel.hw5.service.PigLatinService;
import edu.xavier.csci260.mcnameel.hw5.service.PigLatinServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

/**
 * class ___ is a member of the hw5-2-mcnameel project.
 * <p>
 * Created by Luke on 3/4/2017.
 */
@Component
public class WordTrackerDAOImpl implements WordTrackerDAO {

    private final PigLatinService pigLatinService;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public WordTrackerDAOImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.pigLatinService = new PigLatinServiceImpl();
    }

    /**
     * verify that the conversions match for the word and if they do updates the timesSeen by 1. If the conversions
     * don’t match log it and throw a word conversion exception.
     * @param word the word to update
     * @param conversion the conversion of word
     */
    @Override
    public void wordUpdate(String word, String conversion) throws WordConversionException {

        Optional<List<WordStats>> listOfWords = find(word);

        if (!listOfWords.isPresent() || !pigLatinService.translate(word).equals(conversion)) {

            throw new WordConversionException(word);

        } else {

            final String updateSql = "UPDATE word_stats SET timesSeen = ? WHERE word = ?";

            List<WordStats> words = listOfWords.get();

            for(int i = words.size() - 1; i >= 0; --i) {

                Integer newTimesSeen = words.get(i).getTimesSeen() + 1;

                jdbcTemplate.update(updateSql, newTimesSeen, word);
            }
        }
    }

    /**
     * select and return an optional list of stats for a word.
     * @param word to find in the database
     * @return an optional list of all of the WordStats of the word
     */
    @Override
    public Optional<List<WordStats>> find(String word) {

        Optional<List<WordStats>> words = Optional.ofNullable(

                jdbcTemplate.query(

                "SELECT word, conversion, timesSeen FROM word_stats WHERE word = ?",
                new Object[]{word}, (rs, rowNum) -> new WordStats(
                     rs.getString("word"),
                     rs.getString("conversion"),
                     rs.getInt("timesSeen")
                    )
                )
        );

        Optional<List<WordStats>> returnMe = Optional.empty();

        if(words.isPresent() && !words.get().isEmpty()) {

            returnMe = words;
        }

        return returnMe;
    }

    /**
     *  creates entry in the database.
     * @param word the word to insert
     * @param conversion the conversion of the word
     */
    @Override
    public void createWords(String word, String conversion) {

        WordStats[] newWord = {new WordStats(word, conversion, 1)};

        List<WordStats> words = asList(newWord);

        jdbcTemplate.batchUpdate("INSERT INTO word_stats(word, conversion, timesSeen) VALUES (?,?,?)",

                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {

                        WordStats w = words.get(i);

                        ps.setString(1, w.getWord());
                        ps.setString(2, w.getConversion());
                        ps.setInt(3, 1);
                    }

                    @Override
                    public int getBatchSize() {
                        return words.size();
                    }
                }
        );
    }

    /**
     * @return all records from table in database.
     */
    @Override
    public Optional<List<WordStats>> findAll() {

        return Optional.of((List<WordStats>) jdbcTemplate.query("SELECT * FROM word_stats",

                new BeanPropertyRowMapper(WordStats.class)));
    }
}
