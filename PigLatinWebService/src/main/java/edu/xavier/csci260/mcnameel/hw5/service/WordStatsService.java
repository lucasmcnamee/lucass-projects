package edu.xavier.csci260.mcnameel.hw5.service;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;

import java.util.List;
import java.util.Optional;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/6/2017.
 */
public interface WordStatsService {

    /**
     *     check to see if word exists in database, if not create word with count of 1 and save in database.
     *     If word does exist, call  the wordUpdate method
     * @param wordStats the word to update
     */
    Optional<List<WordStats>> update(WordStats wordStats);

    /**
     *     This takes an optional parameter, if optional is populated call find other wise call findAll
     * @param checkMe the optional to check
     */
    Optional<List<WordStats>> findWord(Optional<String> checkMe);
}
