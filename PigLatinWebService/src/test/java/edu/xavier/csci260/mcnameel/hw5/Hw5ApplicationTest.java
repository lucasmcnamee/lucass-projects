package edu.xavier.csci260.mcnameel.hw5;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class Hw5ApplicationTest {


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void piglatinTranslateWordInDBStatusOk() throws Exception {
        mockMvc.perform(get("/piglatin/translate/word"))
                .andExpect(status().isOk());
    }

    @Test
    public void piglatinTranslateWordNotInDBStatusOk() throws Exception {
        mockMvc.perform(get("/piglatin/translate/wordInvalid"))
                .andExpect(status().isOk());
    }

    @Test
    public void wordStatsWordStatusOk() throws Exception {
        mockMvc.perform(get("/word/stats/word"))
                .andExpect(status().isOk());
    }

    @Test
    public void wordStatsAllStatusOk() throws Exception {
        mockMvc.perform(get("/word/all"))
                .andExpect(status().isOk());
    }

   /* @Test
    public void wordUpdateWordInDBStatusOk() throws Exception {
        mockMvc.perform(get("/word/update", "word"))
                .andExpect(status().isOk());
    }

    @Test
    public void wordUpdateWordNotInDBStatusOk() throws Exception {
        mockMvc.perform(get("/word/update", "wordInval"))
                .andExpect(status().isOk());
    }*/
}
