package edu.xavier.csci260.mcnameel.hw5.controller;

import edu.xavier.csci260.mcnameel.hw5.dal.WordTrackerDAOImpl;
import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import edu.xavier.csci260.mcnameel.hw5.service.WordStatsService;
import edu.xavier.csci260.mcnameel.hw5.service.WordStatsServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/5/2017.
 */
public class WordStatsControllerTest {

    private WordStatsController sut;
    private WordStatsService service = new WordStatsServiceImpl(mock(WordTrackerDAOImpl.class));

    @Before
    public void setUp() throws Exception {
        sut = new WordStatsController(service);
    }

    @Test
    public void getAll() throws Exception {

        WordStats[] useMe = new WordStats[]{new WordStats("word", "ord way",1),
                                            new WordStats("word2", "ord way2",2)};

        Optional<List<WordStats>> stats = Optional.of(Arrays.asList(useMe));

        when(service.findWord(Optional.empty())).thenReturn(stats);

        assertEquals("[WordStats[word='word', conversion='ord way', timesSeen=1], " +
                "WordStats[word='word2', conversion='ord way2', timesSeen=2]]", sut.getAll().toString());
    }

    @Test
    public void post() throws Exception {

        WordStats wordStats1 = new WordStats("word", "ord way",1);

        WordStats[] wordStats2 = {new WordStats("word", "ord way",2)};

        Optional<List<WordStats>> stats = Optional.of(Arrays.asList(wordStats2));

        when(service.update(wordStats1)).thenReturn(stats);

        assertEquals("[WordStats[word='word', conversion='ord way', timesSeen=2]]",
                sut.post(wordStats1));

    }

    @Test
    public void get() throws Exception {

        WordStats[] useMe = new WordStats[]{new WordStats("word", "ord way",1)};

        Optional<List<WordStats>> stats = Optional.of(Arrays.asList(useMe));

        when(service.findWord(Optional.of("word"))).thenReturn(stats);

        assertEquals("[WordStats[word='word', conversion='ord way', timesSeen=1]]", sut.get("word").toString());
    }

}