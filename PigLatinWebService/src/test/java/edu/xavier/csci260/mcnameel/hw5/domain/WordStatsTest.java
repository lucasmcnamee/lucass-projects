package edu.xavier.csci260.mcnameel.hw5.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * class ___ is a member of the hw5-2 project.
 * <p>
 * Created by Luke on 3/21/2017.
 */
public class WordStatsTest {

    private WordStats sut;

    @Before
    public void setUp() throws Exception {
        sut = new WordStats("word", "ord way", 1);
    }

    @Test
    public void toStringTest() throws Exception {

        assertEquals("WordStats[word='word', conversion='ord way', timesSeen=1]", sut.toString());
    }

    @Test
    public void getWord() throws Exception {

        assertEquals("word", sut.getWord());
    }

    @Test
    public void setWord() throws Exception {

        sut.setWord("newWord");

        assertEquals("newWord", sut.getWord());
    }

    @Test
    public void getConversion() throws Exception {

        assertEquals("ord way", sut.getConversion());
    }

    @Test
    public void setConversion() throws Exception {

        sut.setConversion("newConversion");

        assertEquals("newConversion", sut.getConversion());
    }

    @Test
    public void getTimesSeen() throws Exception {

        assertEquals(1, (int) sut.getTimesSeen());
    }

    @Test
    public void setTimesSeen() throws Exception {

        sut.setTimesSeen(2);

        assertEquals(2, (int) sut.getTimesSeen());
    }

}