package edu.xavier.csci260.mcnameel.hw5.controller;

import edu.xavier.csci260.mcnameel.hw5.dal.WordTrackerDAO;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;


/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/10/2017.
 */
public class PigLatinControllerTest {

    private PigLatinController sut;

    @Before
    public void setUp() throws Exception {
        sut = new PigLatinController(mock(WordTrackerDAO.class));
    }

    @Test
    public void post() throws Exception {
        assertEquals("ord way", sut.post("word"));
    }

}