package edu.xavier.csci260.mcnameel.hw5.service;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/11/2017.
 */
@SuppressWarnings("ALL")
@SpringBootTest
@RunWith(SpringRunner.class)
public class WordStatsServiceTest {

    private JdbcTemplate jdbcTemplate;

    private WordStatsService sut;

    @Autowired
    public void setUp(JdbcTemplate jdbcTemplate, WordStatsService wordStatsService) {

        sut = wordStatsService;

        this.jdbcTemplate = jdbcTemplate;
    }

    @Test
    public void updateNewWord() throws Exception {

        List<WordStats> words = sut.update(new WordStats("invalid", "",0)).get();

        assertEquals("", words.get(0).getConversion());
    }

    @Test
    public void updateExistingWord() throws Exception {

        List<WordStats> words = sut.update(new WordStats("word", "ord way", 1)).get();

        assertEquals("ord way", words.get(0).getConversion());
    }

    @Test
    public void findWordExistingWord() throws Exception {

        List<WordStats> words = sut.findWord(Optional.of("word")).get();

        assertEquals("word", words.get(0).getWord());
    }

    @Test
    public void findWordNonexistingWord() throws Exception {

        List<WordStats> words = sut.findWord(Optional.empty()).get();

        assertTrue(1 < words.size());
    }
}