package edu.xavier.csci260.mcnameel.hw5.dal;

import edu.xavier.csci260.mcnameel.hw5.domain.WordStats;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * class ___ is a member of the hw5-2 project.
 * <p>
 * Created by Luke on 3/20/2017.
 */
@SuppressWarnings("ALL")
@RunWith(SpringRunner.class)
@SpringBootTest
public class WordTrackerDAOTest {

    @Autowired
    private WordTrackerDAO sut;


    @Test
    public void wordUpdateWordConversionException() throws Exception {

        boolean returnMe = false;

        try {

            sut.wordUpdate("valid", "invalid");

        } catch (WordTrackerDAO.WordConversionException w) {

            returnMe = true;

        }

        assertTrue(returnMe);
    }

    @Test
    public void wordUpdateNoWordConversionException() throws Exception {

        boolean returnMe = true;

        try {

            sut.wordUpdate("word", "ord way");

        } catch (WordTrackerDAO.WordConversionException w) {

            returnMe = false;

        }

        assertTrue(returnMe);
    }

    @Test
    public void findStringPresent() throws Exception {

        assertTrue(sut.find("word").isPresent());
    }

    @Test
    public void findStringNotPresent() throws Exception {

        assertTrue(!sut.find("wordaergrwaaegr").isPresent());
    }

    @Test
    public void findExisting() throws Exception {

        List<WordStats> wordStats = sut.find("word").get();

        assertEquals(new WordStats("word", "ord way", 1).toString(),
                wordStats.get(0).toString());
    }

    @Test
    public void createWords() throws Exception {

        WordStats wordStats = new WordStats("valid", "alid vay", 1);

        sut.createWords("valid", "alid vay");

        List<WordStats> wordStatsList = sut.find("valid").get();

        assertEquals(new WordStats("valid", "alid vay", 1).toString(),
                wordStatsList.get(0).toString());
    }
}