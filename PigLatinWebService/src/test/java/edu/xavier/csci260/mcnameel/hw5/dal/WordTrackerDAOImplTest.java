package edu.xavier.csci260.mcnameel.hw5.dal;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * class ___ is a member of the hw5-2-mcnameel(1) project.
 * <p>
 * Created by Luke on 3/4/2017.
 */
public class WordTrackerDAOImplTest {

    private JdbcTemplate jdbcTemplate;

    @Test
    public void wordUpdate() throws Exception {

        boolean success = true;

        WordTrackerDAOImpl sut = new WordTrackerDAOImpl(mock(JdbcTemplate.class));

        try {
            sut.wordUpdate("Hello", "Goodbye");
        } catch (WordTrackerDAO.WordConversionException e) {
            success = false;
        }

        assertEquals(false, success);
    }

    @Test
    public void find() throws Exception {

    }

    @Test
    public void createWord() throws Exception {

    }

    @Test
    public void findAll() throws Exception {

    }
}