package edu.xavier.csci260.mcnameel.hw5.service;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

/**
 * class ___ is a member of the hw5-2 project.
 * <p>
 * Created by Luke on 3/20/2017.
 */
@SpringBootTest
public class PigLatinServiceImplTest {

    private PigLatinService sut = new PigLatinServiceImpl();

    @Test
    public void translate() throws Exception {

        assertEquals("ord way", sut.translate("word"));
    }
}