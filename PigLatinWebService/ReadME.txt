 ====================================================================================================================
 +------------------------------------------------PigLatinWebService------------------------------------------------+
 +---------------------------------------------------Luke McNAmee---------------------------------------------------+
 +                                                                                                                  +
 + PigLatinWebService is a fully functional Web application created in Java using the SpringBoot framework. This web+
 + service handles different type of requests either throught a URL or passing Json objects through the HTTP request+
 + body. This webservice has been fully tested with Junit for unit testing and uses Mockito and Springframework     +
 + MockMvc, this has also been tested through SonarQube with an "A" rating and "no issues" found                    +
 +                                                                                                                  +
 +------------------------------------------------------------------------------------------------------------------+ 
