package service;

import java.util.HashMap;

/**
 * class Inventory is a member of the Inventory Manager project. Inventory is the main service class.
 *
 * Created by Luke on 2/1/2017.
 */
public class Inventory {
    private HashMap<String, Cans> invent;
    private OrderMaker orderMaker;

    // instantiate
    public Inventory() {
        Cans green = new Cans("Green", new Integer[]{2, 4, 8});
        Cans red = new Cans("Red", new Integer[]{1, 4, 7});
        Cans yellow = new Cans("Yellow", new Integer[]{3, 3});

        orderMaker = new OrderMaker();

        invent = new HashMap<>();
        invent.put("Green", green);
        invent.put("Red", red);
        invent.put("Yellow", yellow);
    }

    /** NOT USED
    // instantiate field vars with param values
    public Inventory(String[] bagOfColors, ArrayList<Integer[]> bagOfSizes) {
        for(int i = 0; i < bagOfSizes.size(); i++) {
            String color = bagOfColors[i];
            invent.put(color, new Cans(color, bagOfSizes.get(i)));
        }
    }

    /**
     * Print the current inventory.
     * @return returnMe - String, the print value is returned for testing purposes
     */
    public String print() {
        return "Current inventory: " + invent.toString();
    }

    /**
     * Check if it is possible to fulfill a specific order.
     */
    public Order canOrder(String color, int size) {
        Order returnMe;
        if(!invent.containsKey(color)) // if the desired color does not exist, create empty order
            returnMe = orderMaker.makeOrder(size, new Cans(color, new Integer[]{}));
        else { // check if there is enough paint of the desired color, and create an order
            Cans can = invent.get(color);
            returnMe = orderMaker.makeOrder(size, can);
        }
        return returnMe;
    }

    /**
     * checkout() - Checkout goods to fulfill an order
     *
     * @param color - String, the desired color of the order
     * @param size - int, the desired size of the order
     */
    public void checkout(String color, int size) {
        Order orderMe = canOrder(color, size);
        if(orderMe.isPossible()) {
            invent.get(color).pushOrder(orderMe);
        }
    }

    /**
     * isInt() - returns true if the string is a number greater than zero
     * @param message - a string to test if it can be an int
     * @return boolean
     */
    public static boolean isInt(String message) {
        try {
            int age = Integer.parseInt(message);
            return (age > 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}