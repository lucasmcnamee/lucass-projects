package service;

import java.util.ArrayList;

/**
 * class OrderMaker is a member of the InventoryManager project. This class checks if an order can be processed with
 *  the current inventory then returns an Object of Order.
 *
 * Created by Luke on 2/5/2017.
 */
class OrderMaker {
    private Integer[] sumsToSize; // will store the numbers which add up to the desired sum

    /**
     * makeOrder() - creates an order whether possible or not possible
     * @param size
     * @param can
     * @return Order
     */
    public Order makeOrder(int size, Cans can) {
        sumsToSize = new Integer[]{};
        Order returnMe;
        if (can.contains(size))  // if there is single can of the desired size within the inventory
            // pass in array with the single value needed for the order
            returnMe = new Order(can.getColor(), true, new Integer[]{size}, size);
        else { // if the size is not within the inventory, begin greedy search
            Integer[] sumToTarget = sumsToSize(can.getNumOfCans(), size, new ArrayList<>());
            if (checkSum(size, sumToTarget))
                returnMe = new Order(can.getColor(), true, sumToTarget, size);
            else // there is no combination of cans that will produce the desired result
                returnMe = new Order(can.getColor(), false, new Integer[]{}, size);
        }
        return returnMe;
    }

    /**
     * sumToSize() - passes data through to sumUp and returns the field var of the list
     * @param numbers - the list of numbers in which to find the sum
     * @param target - desired sum
     * @param partial - pass in new empty list
     * @return - the array holding the the values that sum up to the target
     */
    private Integer[] sumsToSize(ArrayList<Integer> numbers, int target, ArrayList<Integer> partial) {
        sumUp(numbers, target, partial);
        return sumsToSize;
    }

    private void sumUp(ArrayList<Integer> numbers, int target, ArrayList<Integer> partial) {
        int tryingToBTarget = 0;
        for (int x : partial) tryingToBTarget += x;
        if (tryingToBTarget == target) // process completed, we have found the target
            sumsToSize = partial.toArray(new Integer[0]);
        if (tryingToBTarget >= target)
            return;
        for (int i1 = 0; i1 < numbers.size(); i1++) {
            ArrayList<Integer> remaining = new ArrayList<>();
            int n = numbers.get(i1);
            for (int i2 = i1 + 1; i2 < numbers.size(); i2++) remaining.add(numbers.get(i2));
            ArrayList<Integer> partial_rec = new ArrayList<>(partial);
            partial_rec.add(n);
            sumUp(remaining, target, partial_rec);
        }
    }

    /**
     * checkSum() - checks if correctSum equals the sum of the elements of the param array
     * @param correctSum
     * @param checkMeOut
     */
    private static boolean checkSum(Integer correctSum, Integer[] checkMeOut) {
        boolean returnMe = false;
        Integer sum = 0;
        if(checkMeOut != null) {
            for (Integer aCheckMeOut : checkMeOut) {
                sum += aCheckMeOut;
            }
            if(correctSum == sum) returnMe = true;
        }

        return returnMe;
    }
}
