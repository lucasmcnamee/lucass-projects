package service;

import java.util.Arrays;
/**
 * class Order is a member of the InventoryManager project. This class holds a proposed order to be processed or to
 *  provide info addressing why the order could not be processed
 *
 * Created by Luke on 2/1/2017.
 */
public class Order {
    private final String color;
    private final Integer[] size;
    private final Integer orderSize;
    private final boolean isPossible;
    private String orderMsg;

    /**
     * @param color - String, the color of the cans
     * @param size - int, the number of liters within the order
     */
    public Order(String color, boolean possibleColor, Integer[] size, Integer desiredSize) {
        this.color = color;
        this.size = size;
        this.orderSize = desiredSize;
        if(!possibleColor) {
            isPossible = false;
            setOrderMsg(false);
        }
        else if(size.length == 0) {
            isPossible = false;
            setOrderMsg(true);
        }
        else {
            isPossible = true;
            setOrderMsg(true);
        }
    }

    private void setOrderMsg(boolean hasColor) {
        if(isPossible()) { // order is possible
            orderMsg = "Checked out the following cans to fulfill the order of " + orderSize + " liter(s) of " + color +
                    " paint: " + Arrays.toString(size);
        }
        else if(hasColor) { // if order is not possible but color is valid
            orderMsg = "Not possible to fulfill the order of " + orderSize + " liter(s) of " + color + " paint with " +
                    "current inventory.";
        }
        else // order is not possible because the color is not recognized
            orderMsg = "Not possible to fulfill any order of " + color + " paint with current inventory.";
    }

    /**
     * orderMessage() - generates a String which will contain the amount of cans used in the order if isPossible(),
     *  otherwise the String will contain the reason the order could not be completed.
     * @return String - the order message
     */
    public String orderMsg() { return orderMsg; }

    public boolean isPossible() { return isPossible; }

    public Integer[] getSize() {
        return size;
    }
}
