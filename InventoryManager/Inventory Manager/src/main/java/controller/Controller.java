package controller;

import service.Inventory;
import javafx.application.Application;
import javafx.stage.Stage;
/**
 * class Controller is a member of the InventoryManager project and starts the program.
 *
 * Created by Luke on 2/6/2017.
 */
public class Controller extends Application {
    public static void main(String[] args) {
        launch(args); // launches
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new ManagerWindow(primaryStage, new Inventory());
    }
}