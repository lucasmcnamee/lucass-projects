package controller;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * class AlertBox is a member of the InventoryManager project. Alerts the user through a pop-up window
 *
 * Created by Luke on 2/4/2017.
 */
class AlertBox {

    public static void display(String title, String message) {
        Stage window = new Stage();

        // this functionality forces the user to use take care of this window first
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(400);
        window.setMinHeight(150);

        // Label of AlertBox
        Label label = new Label();
        label.setText(message);

        // Button on AlertBox
        Button closeButton = new Button("Close the window");
        closeButton.setOnAction(e -> window.close());

        // Layout of AlertBox
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);

        // Set the Scene
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait(); // works with Modality to force this window to be taken care of before further action
    }
}
