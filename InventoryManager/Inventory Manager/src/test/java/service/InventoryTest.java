package service;

import org.junit.Before;
import org.junit.Test;
import service.Inventory;

import static org.junit.Assert.*;

/**
 * class service.InventoryTest is a member of the service.Inventory Manager project.
 * <p>
 * Created by Luke on 2/1/2017.
 */
public class InventoryTest {
    private Inventory manageMe;
    private Object output;

    @Before
    public void setUp() throws Exception {
        manageMe = new Inventory();
    }


    /* Tests for print() - 1 case */
    @Test
    public void print() throws Exception {
        output = manageMe.print();
        assertEquals("Current inventory: {Red=[1, 4, 7], Yellow=[3, 3], Green=[2, 4, 8]}", output);
    }

    /* Tests for canOrder() | 4 cases */
    // case 1: input color and size are valid for an order
    @Test
    public void canOrderValid() throws Exception {
        output= manageMe.canOrder("Green", 8).isPossible();
        assertEquals(true, output);
    }

    // case 2: input color is valid but size invalid for an order
    @Test
    public void canOrderInvalid() throws Exception {
        output = manageMe.canOrder("Red", 3).isPossible();
        assertEquals(false, output);
    }

    // case 3: input color is invalid for an order
    @Test
    public void canOrderInvalOrder() throws Exception {
        output = manageMe.canOrder("Purple", 8).isPossible();
        assertEquals(false, output);
    }

    // case 4: input color and size are valid but the order requires multiple cans
    @Test
    public void canOrderValidMultiCans() throws Exception {
        output = manageMe.canOrder("Red", 5).isPossible();
        assertEquals(true, output);
    }

    /* Tests for checkout() | 4 *
    // case 1:
    @Test
    public void checkoutValid() throws Exception {
        output = manageMe.checkout("Green", 8);
        assertEquals(true, output);
    }*/
}