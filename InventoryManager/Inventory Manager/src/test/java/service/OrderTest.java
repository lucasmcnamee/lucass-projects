package service;

import org.junit.Test;
import service.Order;

import static org.junit.Assert.*;

public class OrderTest {
    private Order orderMe;
    private Object output;

    /* Tests for setOrderMsg() | 3 cases */
    // test if service.Order was constructed properly
    // case 1: service.Order with valid color and size
    @Test
    public void getOMVal() throws Exception {
        orderMe = new Order("Blue", true, new Integer[]{1,2,3}, 5);
        output = orderMe.orderMsg();
        assertEquals("Checked out the following cans to fulfill the order of 5 liter(s) of Blue paint: " +
                "[1, 2, 3]", output);
    }

    // case 2: service.Order with valid color but invalid size
    @Test
    public void getOMInvalSize() throws Exception {
        orderMe = new Order("Blue", true, new Integer[0], 5);
        output = orderMe.orderMsg();
        assertEquals("Not possible to fulfill the order of 5 liter(s) of Blue paint with current " +
                "inventory.", output);
    }

    // case 3: service.Order with invalid color but valid size
    @Test
    public void getOMInvalColor() throws Exception {
        orderMe = new Order("", false, new Integer[]{1,2,3}, 5);
        output = orderMe.orderMsg();
        assertEquals("Not possible to fulfill any order of  paint with current inventory.", output);
    }

    // case 4: service.Order with invalid color and valid size
    @Test
    public void getOMInvalAll() throws Exception {
        orderMe = new Order("", false, new Integer[0], 5);
        output = orderMe.orderMsg();
        assertEquals("Not possible to fulfill any order of  paint with current inventory.", output);
    }
}
