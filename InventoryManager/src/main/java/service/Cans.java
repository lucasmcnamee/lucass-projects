package service;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * class Cans is a member of the InventoryManager project. This class holds all of the inventory items of the specified
 *  color
 *
 * Created by Luke on 2/1/2017.
 */
class Cans {
    private final String color;
    private ArrayList<Integer> numOfCans;

    /**
     * @param color - String, the color of the cans
     * @param inputCans - int[], the number of liters within the order
     */
    public Cans(String color, Integer[] inputCans) {
        numOfCans = new ArrayList<>(Arrays.asList(inputCans));
        this.color = color;
    }

    /**
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param size
     * @return boolean - if size exists within the inventory
     */
    public boolean contains(int size) {
        return (numOfCans.contains(size));  // if there is single can of the desired size within the inventory
    }

    private void remove(int size) {
        numOfCans.remove(numOfCans.indexOf(size));
    }

    /**
     * pushOrder() - checks if it is possible to complete an order then removes the corresponding inventory.
     *
     * @param orderMe - the order to be processed
     * @return boolean - if the order was successful
     */
    public boolean pushOrder(Order orderMe) {
        boolean returnMe = false; // returns true if order is successful
        if(orderMe.isPossible()) {
            Integer[] arr = orderMe.getSize();
            for (int i : arr) {
                remove(i);
            }
            returnMe = true;
        }
        return returnMe;
    }

    /**
     * toString() - override toString
     * @return String - toString on the ArrayList
     */
    public String toString() {
        return numOfCans.toString();
    }

    /**
     * getNumOfCans
     * @return the different sizes of this color of cans
     */
    public ArrayList<Integer> getNumOfCans() {
        return numOfCans;
    }
}

