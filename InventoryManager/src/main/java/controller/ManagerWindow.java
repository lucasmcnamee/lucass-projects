package controller;

import service.Inventory;
import service.Order;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * class ManagerWindow is a member of the InventoryManager project. This class controls all UI and passes info to the
 *  correct service classes.
 *
 * Created by Luke on 2/7/2017.
 */
class ManagerWindow {
    private GridPane grid;
    private TextField orderColInput;
    private TextField orderSizeInput;

    private Text inventDisplay;

    private Inventory curInvent;

    ManagerWindow(Stage primaryStage, Inventory inventory) {
        // set the initial values
        Stage window = primaryStage;
        curInvent = inventory;

        // layout
        window.setTitle("Inventory Input");
        getGridDisp();

        // add button
        Button buttonOrder = new Button("Place Order");
        buttonOrder.setOnAction(e -> handleOrder());
        grid.add(buttonOrder, 1, 5);

        // set layout to display
        Scene scene = new Scene(grid, 700, 300); // check video on embedding
        window.setScene(scene);
        window.show();
    }

    /**
     * getGridDisp - sued to set the layout of the current window
     */
    private void getGridDisp() {
        // layout properties
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        updateInvent(); // update the window to display the current user inventory
        setTextFields(); // add the text fields to display user input information
    }

    /**
     * updateInvent() - updates the inventory display to match the current Inventory
     */
    private void updateInvent() {
        // remove the previous inventory display message
        grid.getChildren().remove(inventDisplay);

        // update the message
        inventDisplay = new Text(curInvent.print());
        inventDisplay.setFont(Font.font("Tahoma", FontWeight.NORMAL, 18));

        // add the display back to the layout
        grid.add(inventDisplay, 0, 0, 1, 1);
    }

    /**
     * setTextFields - sets the Labels and TextFields to prompt and collect information from and to the user
     */
    private void setTextFields() {

        Text infoText = new Text("Please enter an order");
        infoText.setFont(Font.font("Tahoma", FontWeight.NORMAL, 18));
        grid.add(infoText, 0, 2, 1, 1);

        Label color = new Label("Order Color: (case sensitive)");
        grid.add(color, 0, 3);

        orderColInput = new TextField();
        orderColInput.setPromptText("yellow");
        grid.add(orderColInput, 1, 3);

        Label size = new Label("Order Size:");
        grid.add(size, 0, 4);

        orderSizeInput = new TextField();
        orderSizeInput.setPromptText("6");
        grid.add(orderSizeInput, 1, 4);
    }

    /**
     * handleOrder - used by buttonOrder to pass order information through the Inventory curInvent
     */
    private void handleOrder() {
        // ensure input is valid
        if (!Inventory.isInt(orderSizeInput.getText()))
            AlertBox.display("Invalid Input", "Error: the size must be an integer value " +
                    "greater than zero!");
        else{ // else proceed normally
            String color = orderColInput.getText();
            int size = Integer.parseInt(orderSizeInput.getText()); // convert the String to Integer
            Order order = curInvent.canOrder(color, size);

            if (order.isPossible()) { // check if the order can be completed with current inventory
                curInvent.checkout(color, size);
                updateInvent(); // update to new inventory
                AlertBox.display("Order Completed", order.orderMsg()); // AlertBox to confirm success
            }
            else // else prompt user to order something else
                AlertBox.display("Invalid Input", order.orderMsg());
        }
    }
}
