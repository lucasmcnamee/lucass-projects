package service;

import org.junit.Before;
import org.junit.Test;
import service.Cans;
import service.Order;
import service.OrderMaker;

import static org.junit.Assert.*;

/**
 * class ___ is a member of the mcnameel project.
 * <p>
 * Created by Luke on 2/5/2017.
 */
public class CansTest {
    private Cans can;
    private Object output;
    private OrderMaker orderMaker;
    @Before
    public void setUp() throws Exception {
        can = new Cans("Green", new Integer[]{2, 2, 4, 8});
        orderMaker = new OrderMaker();
    }

    /* Tests for contains() | 2 cases */
    // case 1: the can does contain the size with a single can
    @Test
    public void containsTrue() throws Exception {
        output = can.contains(2);
        assertEquals(true, output);
    }

    // case 2: the can does not contain the size with a single can
    @Test
    public void containsFalse() throws Exception {
        output = can.contains(3);
        assertEquals(false, output);
    }

    /* Test for toString() | 1 case */
    // case 1
    @Test
    public void toStringTest() throws Exception {
        output = can.toString();
        assertEquals("[2, 2, 4, 8]", output);
    }

    /* Tests for pushOrder() | 2 cases */
    // case 1: order is valid
    @Test
    public void pushOrderVal() throws Exception {
        Order orderMe = orderMaker.makeOrder(6, can);
        can.pushOrder(orderMe);
        output = can.toString();
        assertEquals("[2, 8]", output);
    }

    // case 2: order is invalid. This test case should fail.
    @Test
    public void pushOrderInval() throws Exception {
        Order orderMe = orderMaker.makeOrder(7, can);
        output = can.pushOrder(orderMe);
        assertEquals(false, output);
    }

}