package service;

import org.junit.Before;
import org.junit.Test;
import service.Cans;
import service.Order;
import service.OrderMaker;

import static org.junit.Assert.*;

public class OrderMakerTest {
    private OrderMaker orderMaker;
    private Order order;
    private Cans can;
    Integer size;
    String valColor;
    String invalColor;
    Integer[] valArr;
    Integer[] emptyArr;


    @Before
     public void setUp() throws Exception {
        String valColor = "";
        String invalColor = "Blue";
        orderMaker = new OrderMaker();
    }

    /* Tests for makeOrder | 3 cases */
    // case 1: valid order with a single can
    @Test
    public void makeOrderSingleCan() throws Exception {
        can = new Cans("Blue", new Integer[]{1,2,6});
        order = orderMaker.makeOrder(2, can);
        assertEquals(true, order.isPossible());
    }

    // case 2: valid order with a multiple cans
    @Test
    public void makeOrderMultiCan() throws Exception {
        can = new Cans("Blue", new Integer[]{10,3,12,6});
        order = orderMaker.makeOrder(9, can);
        assertEquals(true, order.isPossible());
    }

    // case 3: order with invalid size
    @Test
    public void makeOrderNoArr() throws Exception {
        can = new Cans("", new Integer[0]);
        order = orderMaker.makeOrder(2, can);
        assertEquals(false, order.isPossible());
    }
}