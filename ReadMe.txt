Hello and welcome to my repository! 

About me: I am a sophomore BS in Computer Science at Xavier University. I love programming and 
sometimes I even spend my free time learning about new languages. I love C++ but I also like Java. 

This is where all of my personal and school projects are stored. 

Lisence info: Feel free to read, edit, and use any of my projects under the standard MIT lisence.

- Lucas McNamee